import client from "prom-client";
import { fastify as Fastify } from "fastify";
import * as dotenv from "dotenv";
import { execCommand } from "../src/app";

dotenv.config();

const port = process.env.PORT;
if (!port) throw new Error("PORT for prometheus not defined");

const tests = [
  // latest apis
  { command: "timestamp get /health", container: "timestamp-api-v3" },
  { command: "storage get /health", container: "storage-api-v3" },
  { command: "ledger get /health", container: "ledger-api-v3" },
  { command: "notifications get /health", container: "notifications-api-v2" },
  { command: "authorisation get /health", container: "authorisation-api-v2" },
  { command: "onboarding get /health", container: "users-onboarding-api-v2" },
  { command: "did get /health", container: "did-registry-api-v3" },
  { command: "datahub get /health", container: "proxy-data-hub-api-v3" },
  { command: "tar get /health", container: "trusted-apps-registry-api-v3" },
  { command: "tir get /health", container: "trusted-issuers-registry-api-v3" },
  { command: "tsr get /health", container: "trusted-schemas-registry-api-v2" },
  { command: "tpr get /health", container: "trusted-policies-registry-api-v2" },

  // besu
  { command: "proxyledger getBlock latest", container: "besu" },

  // cassandra
  {
    command: 'cassandra query ["select * from attribute_storage"]',
    container: "cassandradb",
  },
];

if (process.env.HEALTHCHECK_CHECK_OLD_APIS === "true") {
  tests.push(
    ...[
      { command: "timestamp-old get /health", container: "timestamp-api" },
      { command: "storage-old get /health", container: "storage-api" },
      { command: "ledger-old get /health", container: "ledger-api" },
      {
        command: "notifications-old get /health",
        container: "notifications-api",
      },
      {
        command: "authorisation-old get /health",
        container: "authorisation-api",
      },
      {
        command: "onboarding-old get /health",
        container: "users-onboarding-api",
      },
      { command: "did-old get /health", container: "did-registry-api" },
      { command: "datahub-old get /health", container: "proxy-data-hub-api" },
      {
        command: "tar-old get /health",
        container: "trusted-apps-registry-api",
      },
      {
        command: "tir-old get /health",
        container: "trusted-issuers-registry-api",
      },
      {
        command: "tsr-old get /health",
        container: "trusted-schemas-registry-api",
      },
      {
        command: "tpr-old get /health",
        container: "trusted-policies-registry-api",
      },
    ]
  );
}

if (process.env.HEALTHCHECK_EXTENDED_CHECKS === "true") {
  const didIssuer = process.env.HEALTHCHECK_DID_ISSUER;
  if (!didIssuer) throw new Error("HEALTHCHECK_DID_ISSUER not defined");
  tests.push(
    ...[
      { command: "timestamp get /timestamps", container: "timestamp-api-v3" },
      { command: "ledger getBlock latest", container: "ledger-api-v3" },
      { command: "did get /identifiers", container: "did-registry-api-v3" },
      {
        command: `did get /identifiers/${didIssuer}`,
        container: "did-registry-api-v3",
      },
      { command: "tar get /apps", container: "trusted-apps-registry-api-v3" },
      {
        command: "tar get /apps/ledger-api",
        container: "trusted-apps-registry-api-v3",
      },
      {
        command: "tir get /issuers",
        container: "trusted-issuers-registry-api-v3",
      },
      {
        command: `tir get /issuers/${didIssuer}`,
        container: "trusted-issuers-registry-api-v3",
      },
      {
        command: "tsr get /schemas",
        container: "trusted-schemas-registry-api-v2",
      },
    ]
  );
}

async function checkHealth(): Promise<
  {
    name: string;
    value: number;
    duration: number;
  }[]
> {
  const results: { name: string; value: number; duration: number }[] = [];
  for (let i = 0; i < tests.length; i += 1) {
    const test = tests[i];
    let value: number;
    const iniTime = Date.now();
    try {
      await execCommand(test.command);
      value = 1;
    } catch (e) {
      value = 0;
    }
    const result = results.find((r) => r.name === test.container);
    if (result) {
      result.value = result.value && value;
      result.duration += (Date.now() - iniTime) / 1000;
    } else {
      results.push({
        name: test.container,
        value,
        duration: (Date.now() - iniTime) / 1000,
      });
    }
  }
  return results;
}

const register = new client.Registry();

const gaugeHealthApi = new client.Gauge({
  name: "health_container",
  help: "health_container_help",
  labelNames: ["name"],
});

const gaugeDurationApi = new client.Gauge({
  name: "health_container_duration_seconds",
  help: "health_container_duration_seconds_help",
  labelNames: ["name"],
});

const gaugeGlobalDuration = new client.Gauge({
  name: "health_global_duration_seconds",
  help: "health_global_duration_seconds_help",
});

const gaugeGlobalHealth = new client.Gauge({
  name: "health_global",
  help: "health_global_help",
  async collect() {
    const results = await checkHealth();
    let healthGlobal = 1;
    let durationGlobal = 0;
    results.forEach((result) => {
      gaugeHealthApi.set({ name: result.name }, result.value);
      gaugeDurationApi.set({ name: result.name }, result.duration);
      healthGlobal = healthGlobal && result.value;
      durationGlobal += result.duration;
    });
    gaugeGlobalDuration.set({}, durationGlobal);
    this.set({}, healthGlobal);
  },
});

register.registerMetric(gaugeGlobalHealth);
register.registerMetric(gaugeHealthApi);
register.registerMetric(gaugeGlobalDuration);
register.registerMetric(gaugeDurationApi);

const fastify = Fastify({ logger: true });

fastify.get("/metrics", async (req, reply) => {
  await reply
    .header("Content-Type", register.contentType)
    .send(await register.metrics());
});

fastify.get("/health", async (req, reply) => {
  const results = await checkHealth();
  const healthContainers: { [x: string]: number } = {};
  let healthGlobal = 1;
  results.forEach((result) => {
    healthContainers[result.name] = result.value;
    healthGlobal = healthGlobal && result.value;
  });
  await reply.code(healthGlobal ? 200 : 400).send({
    health_global: healthGlobal,
    health_containers: healthContainers,
  });
});

fastify
  .listen(port, "0.0.0.0")
  .then(() => {})
  .catch((error) => {
    fastify.log.error(error);
    process.exit(1);
  });
