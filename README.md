![EBSI Logo](https://ec.europa.eu/cefdigital/wiki/images/logo/default-space-logo.svg)

# Content

- [**Welcome to EBSI Test Scripts**](#welcome)
  - [Installation](#install)
- [**CLI commands reference**](#cli-commands)
  - [set, using, view, env, run, exit](#set-client)
  - [compute](#compute)
  - [Conformance API v3](#conformance)
  - [Conformance API v2](#conformance-old)
  - [Onboarding API](#onboarding-api)
  - [Authorisation API v3](#authorisation-api)
  - [Authorisation API v2](#authorisation-api-old)
  - [Ledger API](#ledger-api)
  - [DID Registry API](#did-registry-api)
  - [Timestamp API](#timestamp-api)
  - [Trusted Apps Registry API](#trusted-apps-registry-api)
  - [Trusted Issuers Registry API](#trusted-issuers-registry-api)
  - [Trusted Schemas Registry API](#trusted-schemas-registry-api)
  - [Storage API](#storage-api)
  - [Proxy Data Hub API](#proxy-data-hub-api)
  - [Notifications API](#notifications-api)
  - [Cassandra](#cassandra)
  - [Wallet Conformance Testing Report](#wct-report)
  - [Old endpoints](#old-endpoints)
- [**Programs**](#programs)
- [**Prometheus**](#prometheus)
- [**Testing**](#testing)
- [**License**](#license)

# Welcome to EBSI Test scripts <a name="welcome"></a>

The EBSI Test-scripts Tool is a Command Line Interface to connect and test some of the EBSI Core Services provided on the European Blockchain Service Infrastructure (EBSI). This first version of the tool is compatible with EBSI version 2.0

IMPORTANT REMARKS: Test-Script is a sample code to help Early Adopters (EA), this is not perfect code (nor audited code) but we share it in beta version to help EA better understanding through real User Code, how to consume EBSI Core Services.

## Installation <a name="install"></a>

The last version of Test-Script is stored on EBSI Bitbucket Public Repo. Find it under: https://ec.europa.eu/cefdigital/code/projects/EBSI/repos/test-scripts/browse. Clone the main branch of that repo on your local computer. The repo is maintained by the EBSI Tech Team and could be updated with any future evolution improvements or bugs fixing.

Enter on the root folder of the package and run the command here under to install it. This will install automatically all the required libraries and packages dependencies:

```sh
yarn install
yarn build
```

To start using the tool, run the following command to launch the command line interface:

```sh
yarn start
```

When the CLI prompts `==>` it will be ready to receive commands.

### Quick start <a name="quick-start"></a>

The [Early Adopter demonstrator](https://ec.europa.eu/cefdigital/wiki/display/BLOCKCHAININT/RFC+-+Early+adopter+demonstrator) contains some useful user journeys when interacting with EBSI using this command line interface.

# CLI commands reference <a name="cli-commands"></a>

This section describe the complete list of commands that can be called in the command line interface.

## Set, Using, View, Env, Run, Exit <a name="set-client"></a>

**`using user <algorithm> <didMethod> <privateKey> <did>`**

Command to load a client in the CLI with one of the supported algorithms (ES256, RS256, EdDSA).

Params:

- **algorithm** (optional): Algorithm. It must be ES256K (default), ES256, RS256, or EdDSA. Use "null" to remove the user.
- **didMethod** (optional): Did method version. It must be "did1" (default, legal entities), or "did2" (natural persons).
- **privateKey** (optional): Private Key in hex string or JWK format. By default it creates a random key. The hex string is only valid if the algorithm selected is ES256K.
- **did** (optional): DID of the client. It should start with "did:ebsi" (or another did method registered in the did registry). There is no specific format for the complete did, but it is recommend to use in the third section a 256-bit number encoded in base58. By default it defines a random DID.

Examples:

- `using user ES256K did1 f19863112d798add7c8df9a100a9ed961e7e7986c2e9042506840c8695e38800 did:ebsi:zkqR9GCLrLYbkubAjuqQZAz`
- `using user ES256 did2`
- `using user ES256 did1 {"kty":"EC","crv":"P-256","x":"...","y":"...","d":"..."} did:ebsi:zkqR9GCLrLYbkubAjuqQZAz keys-2`
- `using user null` (remove user)

**`using ethuser <privateKey> <did>`**

Command to call `using user ES256K <didMethod> <privateKey> <did>`.

**`using app <name> <privateKey>`**

Command to load a Trusted App in the CLI. It also calls the Trusted Apps Registry to get the application id.

Params:

- **name**: Application name
- **privateKey**: Private key in hexadecimal format.

Examples:

- `using app my-app f19863112d798add7c8df9a100a9ed961e7e7986c2e9042506840c8695e38800`

**`using token <token>`**

Command to load a JWT derived from a SIOP authorisation. This JWT will be added in the headers of subsequent calls to the APIs. It's also possible to load a JWT previously saved in the CLI (see `set var`) by referencing its name.

Params:

- **token**: JWT to set in the headers. Leave it empty to remove the token from the headers.

Examples:

- `using token eyJ0eXAiOiJ...`
- `using token mySioptoken`
- `using token token1`
- `using token` (token removed)

**`using oauth2token <token>`**

Command to load a JWT. This JWT is added in the headers when calling Ledger API if the SIOP token is not defined (see `using token <token>`). When a transaction is broadcasted to the blockchain the script will use this token to access Ledger API and it will wait for the transaction to be mined by checking the transaction receipt. It's also possible to load a JWT previously saved in the CLI (see `set var`) by referencing its name.

Params:

- **token**: JWT to set in the headers when accessing Ledger API. Leave it empty to remove the token from the headers.

Examples:

- `using oauth2token eyJ0eXAiOiJ...`
- `using oauth2token myOauth2Token`
- `using oauth2token token1`
- `using oauth2token` (token removed)

**`set <var> <value>`**

Command to set a variable. Use it to load different JWTs in the CLI. You can also store the results when calling the different APIs.

Params:

- **var**: Variable name
- **value**: Value to store

Examples:

- `set token1 eyJhbG...`
- `mytoken: authorisation-old siop`

**`view <id>`**

Command to view the data stored in the internal variables.

Params:

- **id**: ID of the variable

Examples:

- `view token` (JWT used in the headers).
- `view oauth2token` (JWT used in the headers of Ledger API).
- `view user` (view user set with `using user`).
- `view app` (view app set with `using app`).
- `view transactionInfo` (view relevant info related to the most recent transaction built/sent).

**`env <environment>`**

Command to set the environment. By default the CLI starts pointing to Test Environment.

Examples:

- `env test`
- `env pilot`
- `env conformance`

**`run <file> <params>`**

This repository contains a script folder with some scripts that provide you examples of how you can easily run multiple commands in a single execution. The commands should be defined in a file and the path should be relative to the folder "scripts". Store your scripts in the folder scripts/local.

Examples:

- `run registerDidDocument_ES256K_ES256 vcToOnboard`
- `vcOnboardSubAccount: run issue_VerifiableAuthorisationToOnboard myIssuer.did mySubAccount.did vcTAO.url`
- `run local/my-script.txt`

**`exit`**

Command to exit from the CLI.

## Compute <a name="compute"></a>

**`compute wait <seconds>`**

Command to wait some seconds.

Params:

- **seconds**: Value to wait in seconds.

**`compute signTransaction <unsignedTransaction>`**

Command to sign a transaction with the private key defined in `using user`. It's also possible to load it from the saved variables by referencing its name.

Params:

- **unsignedTransaction**: JSON with the unsigned transaction (ethereum).

Examples:

- `compute signTransaction 0xf9...`
- `compute signTransaction utx`
- `sgnTx: compute signTransaction utx`

**`compute signJwt <payload> <alg> <headers>`**

Command to create a JWT.

Params:

- **payload**: Payload of the JWT.
- **alg**: Signing algorithm to be used. It should be ES256K, ES256, RS256, or EdDSA.
- **headers** (optional): Headers of the JWT.

**`compute createPresentationJwt <verifiableCredential>`**

Command to generate a Verifiable Presentation using the "vp_jwt" format. The presentation is signed with the user defined in `using user`.

Params:

- **verifiableCredential**: Verifiable credential. It's also possible to load it from the saved variables by referencing its name.

Examples:

- `compute createPresentationJwt eyJhb...`
- `compute createPresentationJwt {"@context":"..."}`
- `compute createPresentationJwt vc`

**`compute createVcJwt <vcPayload> <jwtPayload> <alg>`**

Command to create a verifiable credential with JWT format. It automatically defines relevant data like context, type, issuance date, and expiration data. The credential is signed with the user defined in `using user`.

Params:

- **vcPayload**: Payload to include in "vc" field. Context, id, type, issuer, issuance/expiration date are automatically generated but then can be overwritten.
- **jwtPayload**: Payload to include outside "vc" field.
- **alg** (optional): Signing algorithm to be used. It should be ES256K (default), ES256, RS256, or EdDSA. And the private key should be defined for that algorithm (see `using user`).

Examples:

- `compute createVcJwt {"credentialSubject":{"id":"did:ebsi:123"}} {"sub":"did:ebsi:123"}`
- `vc: compute createVcJwt {"credentialSubject":{"id":"did:ebsi:123"}} {"sub":"did:ebsi:123"} ES256`

**`compute verifyAuthenticationRequest <request>`**

Command to verify an authentication request by checking the signature and did in the request. It return the `client_id` specified in the request. It's also possible to load it from the saved variables by referencing its name. This command is used during the SIOP authentication with the authorisation API.

Params:

- **request**: JSON containing at least the fields `client_id` and `request` (JWT).

Examples:

- `compute verifyAuthenticationRequest {"client_id":"https//...","request":"eyJ0e..."}`
- `compute verifyAuthenticationRequest request`

**`compute verifySessionResponse <response>`**

Command to verify a session response by checking the nonce and signature in the Ake response. It decrypts and returns the access token contained in the Ake response. For decryption it uses the private key defined in the command `using user`. It's also possible to load the response from the saved variables by referencing its name. This command is used during the SIOP authentication with the authorisation API.

Params:

- **response**: JSON containing the fields `nonce` (reference to check in the response), and `response` (using Ake protocol format).

Examples:

- `compute verifySessionResponse {"nonce":"3a4...","response":{"ake1_enc_payload":"9ed9b3b..."}`
- `compute verifySessionResponse response`

**`compute schemaId <schema> <base>`**

Command to compute the ID of a schema.

Params:

- **schema**: Schema as JSON.
- **base**: It should be `base58btc` (default) or `base16`

**`compute checkStatusList2021CredentialSchema <credential>`**

Command to check if a credential follows the status list 2021 credential schema.

Params:

- **credential**: Credential in JSON format.

**`compute verifyVcJwt <credential>`**

Command to verify a verifiable credential.

Params:

- **credential**: Verifiable credetial in JWT format.

**`compute did2 <jwk>`**

Command to compute the DID v2 (did:key for natural person) based on the Public key JWK.

Params:

- **jwk**: Public key JWK.

**`compute decodeJWT <jwt>`**

Command to decode a jwt. It's also possible to load it from the saved variables by referencing its name.

Params:

- **jwt**: JWT to decode.

Examples:

- `compute decodeJWT eyJ0e...`
- `compute decodeJWT myJwt`

**`compute decodeBase64 <data>`**

Command to decode data encoded in base64. It's also possible to load it from the saved variables by referencing its name.

Params:

- **data**: Data to decode.

Examples:

- `compute decodeBase64 eyJ0e...`
- `compute decodeBase64 myData`

**`compute decodeBase64url <data>`**

Command to decode data encoded in base64url. It's also possible to load it from the saved variables by referencing its name.

Params:

- **data**: Data to decode.

Examples:

- `compute decodeBase64url eyJ0e...`
- `compute decodeBase64url myData`

**`compute decodeHex <data>`**

Command to decode data encoded as hex string. It's also possible to load it from the saved variables by referencing its name.

Params:

- **data**: Data to decode.

Examples:

- `compute decodeHex 0x41df...`
- `compute decodeHex 41df...`

**`compute randomID`**

Command to generate a random ID of 32 bytes expressed as hex string.

Examples:

- `compute randomID`
- `id: compute randomID`

## Conformance v3 <a name="conformance"></a>

Set of instructions to interact with the conformance service V3.

**`conformance get <url>`**

Command to call get endpoints of conformance service.

Params:

- **url**: url path to call.

Examples:

- `conformance get /issuer-mock/.well-known/openid-credential-issuer`

You can also take urls saved in variables and join them:

- `conformance get opIssuer.authorization_server /.well-known/openid-configuration`

**`conformance clientMockInitiate`**

Command to setup a server client in the conformance service by using the private keys defined in `using user`. This should be used only for testing purposes. In production, the wallet should have its own server.

Examples:

- `conformance clientMockInitiate`

**`conformance clientMockUpdateList <statusIndex> <statusListIndex> <value>`**

Command to setup/update the Status List 2021 of the client server in the conformance service. This should be used only for testing purposes. In production, the client should have its own server.

Params:

- **statusIndex**: Index of the path /credentials/status/:id.
- **statusListIndex**: Status List index inside the credential.
- **value**: 0 for valid, 1 for revoked.

**`conformance authMockAuthorize <openIdCredentialIssuer> <openIdConfiguration> <requestedTypes> <alg> <codeVerifier> <issuerState>`**

Command to interact with the /authorize endpoint of the auth-mock module. In this call the client requests access for the required credential. The client's public key must be discoverable through client's openid-configuration through the jwks_uri parameter (see `clientMockInitiate`).

Params:

- **openIdCredentialIssuer**: Open ID data obtained from /issuer-mock/.well-known/openid-credential-issuer.
- **openIdConfiguration**: Open ID configuration obtained from /auth-mock/.well-known/openid-configuration.
- **requestedTypes**: Array containing the requested types for the credential.
- **alg** (optional): Signing algorithm to be used. It should be ES256 (default), ES256K, RS256, or EdDSA. And the private key should be defined for that algorithm (see `using user`).
- **codeVerifier** (optional): code verifier for the PKCE challenge. Used in the Holder Wallet flow.
- **issuerState** (optional): issuer state. Used in the Holder Wallet flow.

**`conformance authMockDirectPostIdToken <openIdCredentialIssuer> <openIdConfiguration> <issuerRequest> <alg>`**

Command to interact with the /direct-post endpoint of auth-mock by sending an ID Token. This command should not be used when the credential requested is a VerifiableAuthorisationForTrustChain, in that case use `authMockDirectPostVpToken`.

Params:

- **openIdCredentialIssuer**: Open ID data obtained from /issuer-mock/.well-known/openid-credential-issuer.
- **openIdConfiguration**: Open ID configuration obtained from /auth-mock/.well-known/openid-configuration.
- **issuerRequest**: Issuer request obtained from the payload of the JWT request received from the /authorize endpoint.
- **alg** (optional): Signing algorithm to be used. It should be ES256 (default), ES256K, RS256, or EdDSA. And the private key should be defined for that algorithm (see `using user`).

**`conformance authMockDirectPostVpToken <openIdConfiguration> <issuerRequest> <vpJwt> <type>`**

Command to interact with the /direct-post endpoint of auth-mock by sending a VP Token. It should be used only when the credential requested is a VerifiableAuthorisationForTrustChain.

Params:

- **openIdConfiguration**: Open ID configuration obtained from /auth-mock/.well-known/openid-configuration.
- **issuerRequest**: Issuer request obtained from the payload of the JWT request received from the /authorize endpoint.
- **vpJwt**: Verifiable presentation containing the VerifiableAuthorisationToOnboard.
- **type** (optional): Type of the presentation definition. It should be "holder" for the Holder Wallet flow, or undefined for the A&A flow.

**`conformance authMockToken <openIdCredentialIssuer> <openIdConfiguration> <code> <alg> <codeVerifier> <type>`**

Params:

- **openIdCredentialIssuer**: Open ID data obtained from /issuer-mock/.well-known/openid-credential-issuer.
- **openIdConfiguration**: Open ID configuration obtained from /auth-mock/.well-known/openid-configuration.
- **code**: Code obtained from the direct post response.
- **alg** (optional): Signing algorithm to be used. It should be ES256 (default), ES256K, RS256, or EdDSA. And the private key should be defined for that algorithm (see `using user`).
- **codeVerifier** (optional): code verifier for the PKCE challenge. Used in the Holder Wallet flow.
- **type** (optional): Type for the query params. It should be "preAuthorised" or "". Used in the Holder Wallet flow.

**`conformance issuerMockCredential <openIdCredentialIssuer> <nonce> <requestedTypes> <alg>`**

The client proceeds with the code flow, and calls the Token Endpoint with the required details and signs client_assertion JWT with client's private keys, which public key counterparts are resolvable through jwks_uri or is in the Client Metadata shared in the pre-registration step.

Params:

- **openIdCredentialIssuer**: Open ID data obtained from /issuer-mock/.well-known/openid-credential-issuer.
- **nonce**: c_nonce obtained from the token response.
- **requestedTypes**: Array containing the requested types for the credential.
- **alg** (optional): Signing algorithm to be used. It should be ES256 (default), ES256K, RS256, or EdDSA. And the private key should be defined for that algorithm (see `using user`).

**`conformance issuerMockDeferred <openIdCredentialIssuer> <acceptanceToken>`**

If the /credential endpoint returned an acceptance_token paramater, the client can call the /credential_deferred endpoint in order to receive the requested credential.

Params:

- **openIdCredentialIssuer**: Open ID data obtained from /issuer-mock/.well-known/openid-credential-issuer.
- **acceptanceToken**: Acceptance token obtained in the /credential endpoint.

**`conformance issuerMockInitiateCredentialOffer <initiateCredentialOfferEndpoint> <credentialType>`**

Command to initiate the credential offer. It returns the credential offer.

Params:

- **initiateCredentialOfferEndpoint**: Endpoint to initiate the credential offer.
- **credentialType**: Credential type to be issued.

**`conformance getCredential <type> <alg> <vc> <issuer>`**

Command to perform a complete flow to get a credential from the issuer mock. The process is the following:

- Call `clientMockInitiate` to setup a server for the client.
- Get Open ID credential issuer from `issuer-mock/.well-known-openid-credential-issuer`.
- Get Open ID configuration from `/.well-known/openid-configuration`.
- Call `authMockAuthorize`
- Create a VP in the case of Root TAO.
- Call `authMockDirectPostIdToken` or `authMockDirectPostVpToken` in the case of Root TAO.
- Call `authMockToken`
- Call `issuerMockCredential`
- Call `issuerMockDeferred` in the case of Root TAO.
- Return the credential

Params:

- **type**: Type of credential requested. It should be "onboard", "ti", "tao", or "roottao".
- **alg** (optional): Signing algorithm to be used. It should be ES256 (default), ES256K, RS256, or EdDSA. And the private key should be defined for that algorithm (see `using user`).
- **vc** (optional): Verifiable credential to support the request.
- **issuer** (optional): Issuer. Use it to connect with the client-mock instead of the issuer-mock.

**`conformance holder <type> <alg> <vc> <typeCredentialOffer> <issuer>`**

Command to perform a complete flow to get a credential as a Holder Wallet. The process is the following:

- Call `issuerMockInitiateCredentialOffer` to initiate the credential offer (see `typeCredentialOffer`).
- Get Open ID credential issuer from `issuer-mock/.well-known-openid-credential-issuer`.
- Get Open ID configuration from `/.well-known/openid-configuration`.
- Call `authMockAuthorize`
- Create a VP in the case of CTWalletQualificationCredential.
- Call `authMockDirectPostIdToken` or `authMockDirectPostVpToken` in the case of CTWalletQualificationCredential.
- Call `authMockToken`
- Call `issuerMockCredential`
- Call `issuerMockDeferred` in the case of deffered credential.
- Return the credential

Params:

- **credentialType**: Type of credential requested. It should be CTWalletCrossInTime, CTWalletCrossDeferred, CTWalletCrossPreAuthorised, CTWalletSameInTime, CTWalletSameDeferred, CTWalletSamePreAuthorised, or CTWalletQualificationCredential.
- **alg** (optional): Signing algorithm to be used. It should be ES256 (default), ES256K, RS256, or EdDSA. And the private key should be defined for that algorithm (see `using user`).
- **vc** (optional): Verifiable credential to support the request.
- **typeCredentialOffer** (optional): Parameter to indicate if the credential offer should be used in the communication or not. It should be `use-credential-offer`, `skip-credential-offer`, or empty which represents `use-credential-offer` (default value). For reference, the skip option is used in the issue to holder flow.
- **issuer** (optional): Issuer. Use it to connect with the client-mock instead of the issuer-mock.

**`conformance check <intent> <preAuthorizedCode>`**

Command to query the /check endpoint of conformance API, which is used to test the wallet.

Params:

- **intent**: Intent to test.
- **preAuthorizedCode** (optional): Preauthorized code. Used in the issue to holder flow.

Examples:

- `conformance check verifier_id_token_exchange`
- `conformance check ti_request_verifiable_authorisation_to_onboard`

## Conformance v2 <a name="conformance-old"></a>

Set of instructions to interact with the conformance service V2. This service will be deprecated, please use conformance v3 instead.

**`conformance-old get <url>`**

Command to GET in conformance service. It is useful to get the conformance logs.

Params:

- **url**: Path to get.

Examples:

- `data: conformance-old get /logs/5fd122e1-c5e5-40a6-b448-00e0f21f9d7e`
- `conformance-old get /logs/ conformanceId`
- `conformance-old get /health`

**`conformance-old setId <uuid>`**

Command to set a conformance id for the headers.

Params:

- **uuid** (optional): UUID identifier. If it is empty it will create a random UUID. If it is "null" it will remove the id.

Examples:

- `conformance-old setId`
- `conformance-old setId d3d701df-513d-46b5-9285-25ea05c639a0`
- `conformance-old setId null`

**`conformance-old issuerInitiate <flowType> <redirect>`**

Command to call the initiate endpoint in the issuer mock in the conformance service.

Params:

- **flowType** (optional): It must be "cross-device" or "same-device" (default).
- **redirect** (optional): boolean defining the redirect option. By default it is false.

Examples:

- `conformance-old issuerInitiate`
- `conformance-old issuerInitiate cross-device true`

**`conformance-old issuerAuthorize <redirectUri> <credentialType>`**

Command to call the authorize endpoint in the issuer mock in the conformance service.

Params:

- **redirectUri**: uri to redirect
- **credentialType**: credential type received in the initiate call

Examples:

- `conformance-old issuerAuthorize http://mywallet.io https://api-pilot.ebsi.eu/trusted-schemas-registry/v1/schemas/0xad457662a535791e888994e97d7b5e0cdd09fbae2c8900039d2ee2d9a64969b1`

**`conformance-old issuerToken <code> <redirectUri>`**

Command to call the token endpoint in the issuer mock in the conformance service.

Params:

- **code**: code received in the authorize call
- **redirectUri** (optional): uri to redirect. By default it is http://localhost:3000

Examples:

- `conformance-old issuerToken abcdfe123456`

**`conformance-old issuerCredential <c_nonce> <accessToken> <alg> <credentialType> <issuerUrl>`**

Command to call the credential endpoint in the issuer mock in the conformance service. It requires an access token (obtained in the previous call) in the headers.

Params:

- **c_nonce**: nonce received in the previous call
- **accessToken**: access token received in the token call
- **alg**: Signing algorithm to be used. It should be ES256K (default), ES256, RS256, or EdDSA. And the private key should be defined for that algorithm (see `using user`).
- **credentialType**: credential type received in the initiate call
- **issuerUrl**: issuer url received in the initiate call

Examples:

- `conformance-old issuerCredential 7aba0bd957099f28fd31 2f6a857923f6565b3432 ES256K https://api-test.ebsi.eu/trusted-schemas-registry/v1/schemas/0xad457662a535791e888994e97d7b5e0cdd09fbae2c8900039d2ee2d9a64969b1 https://conformance-test.ebsi.eu/conformance/v2`

**`conformance-old verifierAuthRequest <flowType> <redirect>`**

Command to call the endpoint /authentication-requests in the verifier-mock.

Params:

- **flowType** (optional): It must be "cross-device" or "same-device" (default).
- **redirect** (optional): boolean defining the redirect option

Examples:

- `conformance-old verifierAuthRequest`
- `conformance-old verifierAuthRequest cross-device true`

**`conformance-old verifierAuthResponse <jwtVp> <alg>`**

Command to call /authentication-responses in the verifier mock.

Params:

- **jwtVp**: presentation to include in the data. It should be a presentation in JWT (see `compute createPresentationJwt`)

- **alg** (optional): Signing algorithm to be used for the id token. It should be ES256K (default), ES256, RS256, or EdDSA. And the private key should be defined for that algorithm (see `using user`).

Examples:

- `conformance-old verifierAuthResponse eyJ0e...`
- `conformance-old verifierAuthResponse eyJ0e... ES256K`

**`conformance-old issuer`**

Command to test the flow in the issuer mock in order to create a credential. It will call internally `issuerAuthorize`, `issuerToken`, and `issuerCredential`.

**`conformance-old verifier`**

Command to test the flow for issuer and verifier. It will call internally the issuer endpoints to create a credential, and the verifier to send a verifiable presentation.

**`conformance-old notification <endpoint> <headers> <body>`**

Command to make a call to conformance-old api through notifications api.

Params:

- **endpoint**: Endpoint in conformance
- **headers**: JSON with the headers
- **body**: JSON with the body

Examples:

- `conformance-old /issuer-mock/authorize {"conformance":"cc08e8cd-6c98-43e1-99ca-b96300eb9ab5"} {"scope":"openid conformance_testing","response_type":"code","redirect_uri":"http://localhost:3000","client_id":"http://localhost:3000","response_mode":"post","state":"73ccb1b10f8f","nonce":"017a42792de3"}`

## Users Onboarding API <a name="onboarding-api"></a>

**`onboarding get <url>`**

OpenAPI documentation: https://api-pilot.ebsi.eu/docs/apis/users-onboarding/latest

Command to GET in Onboarding API. It is useful to check the health of the api.

Params:

- **url**: Path to get.

Examples:

- `onboarding get /health`

**`onboarding authenticationRequests`**

Command to start a SIOP authentication with onboarding api.

Examples:

- `onboarding authenticationRequests`
- `response: onboarding authenticationRequests`

**`onboarding sendAuthResponse <alg>`**

Command to create and send an authentication response. Call this command after going to the Onboarding web page ([Test env](https://app-test.ebsi.eu/users-onboarding/v2) or [Pilot env](https://app-pilot.ebsi.eu/users-onboarding/v2)) and after setting the session token using `using token <token>`. It returns the Verifiable Credential contained in the Verifiable Authorisation received from the API.

Params:

- **alg** (optional): Signing algorithm to be used. It should be ES256K (default), ES256, RS256, or EdDSA. And the private key should be defined for that algorithm (see `using user`).

Examples:

- `onboarding sendAuthResponse`
- `vc: onboarding sendAuthResponse`
- `vc: onboarding sendAuthResponse EdDSA`

**`onboarding authentication <alg>`**

Command to onboard new users in the system. It returns the access token received from the authorisation api. The command will perform the following steps:

- call `onboarding sendAuthResponse <alg>` to send a response and receive back a verifiable credential packaged in a verifiable authorisation.
- call `compute createPresentation <alg>` to create a verifiable presentation using the verifiable credential from the previous step.
- call `compute canonicalizeBase64url {...}` to encode the verifiable presentation in base64url.
- call `authorisation-old siopRequest` to get a request from the authorisation API.
- call `compute verifyAuthenticationRequest` to verify the request.
- call `authorisation-old siopSession` selecting the alg and sending the verifiable presentation.
- call `compute verifySessionResponse` to verify the response from the authorisation api.

Examples:

- `onboarding authentication`
- `mySiopToken: onboarding authentication`
- `mySiopToken: onboarding authentication EdDSA`

_DISCLAIMER: In EBSI v2.0, an instance of the Users Onboarding service is deployed on the pre-production network to enable early adopters access the protected EBSI resources and start easily testing them. In the future, any EOS (ESSIF Onboarding Service) will be able to implement their own onboarding service following the technical specification defined in the EBSI Users Onboarding reference implementation. For production, every use case will be responsible to select the legal entities that will play the role of Onboarding Services. Use Case will be responsible to define the specific business/legal requirements for their onboarding, to define their specific administrative processes. Each use case will have the freedom to implement its own Business Onboarding Service (with linked processes) but their solution should be compliant with the technical requirements described in the reference implementation._ **Nonetheless, the current status of pre-production network does not support the option of providing other onboarding services than Users Onboarding service. Please, use Users Onboarding service provided by EBSI until new notice.**

## Authorisation API v3 <a name="authorisation-api"></a>

OpenAPI documentation: https://api-pilot.ebsi.eu/docs/apis/authorisation/latest

**`authorisation get <url>`**

Command to GET in Authorisation API. It is useful to get the Open ID configuration.

Params:

- **url**: Path to get.

Examples:

- `authorisation get /health`
- `authorisation get /.well-known/openid-configuration`
- `authorisation get /jwks`
- `authorisation get /presentation-definitions?scope=openid%20tir_write`

**`authorisation token <definitionId> <vpJwt>`**

Command request an access token to the authorisation api.

Params:

- **definitionId**: It must be "didr_invite_presentation", "didr_write_presentation", "tir_invite_presentation", or "tir_write_presentation".
- **vpJwt**: Verifiable presentation to include in the call.

Examples:

- `authorisation didr_write_presentation ey...`

**`authorisation auth <definitionId> <alg> <vc>`**

Command to establish a session with authorisation api. It uses the client defined in `using user`. It returns an object containing the access token. The command will perform the following steps:

- call `authorisation get /.well-known/openid-configuration` to get the Open ID configuration.
- call `compute createPresentationJwt` to create a presentation and sign it with the user credentials.
- call `authorisation token` to request the access token.

Params:

- **definitionId**: It must be "didr_invite_presentation", "didr_write_presentation", "tir_invite_presentation", or "tir_write_presentation".
- **alg** (optional): Signing algorithm to be used. It should be ES256K (default), ES256, RS256, or EdDSA.
- **vc** (optional): Verifiable credential to include in the presentation. For an empty vc set "empty" (default value).

Examples:

- `authorisation auth didr_invite_presentation ES256 vcToOnboard`
- `authorisation auth didr_write_presentation ES256`
- `authorisation auth tir_invite_presentation ES256`
- `authorisation auth tir_write_presentation ES256`

## Authorisation API v2 <a name="authorisation-api-old"></a>

OpenAPI documentation: https://api-pilot.ebsi.eu/docs/apis/authorisation/v2

**`authorisation-old get <url>`**

Command to GET in Authorisation API. It is useful to check the health of the api.

Params:

- **url**: Path to get.

Examples:

- `authorisation-old get /health`

**`authorisation-old siopRequest`**

Command to start a SIOP authentication with authorisation api. The authorisation api will return a request.

Examples:

- `authorisation-old siopRequest`
- `response: authorisation-old siopRequest`

**`authorisation-old siopSession <url> <alg> <vpBase64url>`**

Command to call /siop-sessions in authorisation api. It uses the client defined in `using user` to sign the response. It returns the response received from the api. It's also possible to load the parameters from the saved variables by referencing the names.

Params:

- **url**: callback url to build the response.
- **alg** (optional): Signing algorithm to be used. It should be ES256K (default), ES256, RS256, or EdDSA. And the private key should be defined for that algorithm (see `using user`).
- **vpBase64url** (optional): Data to include in the verified claims. It should be canonicalized and encoded in base64url (see `compute canonicalizeBase64url`)

Examples:

- `authorisation-old siopSession https://... ES256K eyJ0e...`
- `authorisation-old siopSession callbackUrl ES256K myVpBase64url`

**`authorisation-old siop <alg> <vcJwtEOS>`**

Command to establish a SIOP session with authorisation api. It uses the client defined in `using user`. It returns an access token. The command will perform the following steps:

- call `compute createPresentationJwt` to create a verifiable presentation (only if a verifiable authorisation is provided).
- call `authorisation-old siopRequest` to get a request from the API.
- call `compute verifyAuthenticationRequest` to verify the request.
- call `authorisation-old siopSession` to send a response to the api.
- call `compute verifySessionResponse` to verify the response and get the access token.

Params:

- **alg** (optional): Signing algorithm to be used. It should be ES256K (default), ES256, RS256, or EdDSA. And the private key should be defined for that algorithm (see `using user`).

- **vcJwtEOS** (optional): Verifiable Authorisation issued by an EBSI Onboarding Service (EOS) to be wrapped in a verifiable presentation. This credential is required for natural persons.

Examples:

- `authorisation-old siop`
- `mySiopToken: authorisation-old siop`
- `mySiopToken: authorisation-old siop EdDSA`
- `mySiopToken: authorisation-old siop ES256K ey...`

**`authorisation-old oauth2 <audience>`**

Command to establish an Oauth2 session with authorisation api. It uses the ap defined in `using app`. It returns an access token. Normally this call is used together with the option to store the results.

Params:

- **audience**: App audience of oauth2 token.

Examples:

- `authorisation-old oauth2 ledger-api`
- `myOauth2Token: authorisation-old oauth2 ledger-api`

## Ledger API <a name="ledger-api"></a>

OpenAPI documentation: https://api-pilot.ebsi.eu/docs/apis/ledger/latest

**`ledger <contract> <method> <params>`**

Command to interact with the smart contracts through the Ledger API without calling other APIs. The contract, methods, and params are the same defined in the previous sections for DID API, Timestamp API, Trusted Apps Registry API, Trusted Issuers Registry API, and Trusted Schemas Registry API.

Examples:

- `ledger timestamp timestampHashes`
- `ledger timestamp build-timestampHashes`
- `ledger tir insertIssuer did:ebsi:zkqR9GCLrLYbkubAjuqQZAz {"institution":"eu"}`

**`ledger getBlock <number>`**

Command to get a block by number. The JWT defined in `using oauth2token` is set in the headers.

Params:

- **number**: Block number

Examples:

- `ledger get 123`

**`ledger getTransactionCount <address>`**

Command to get the number of transactions of a particular address. The JWT defined in `using oauth2token` is set in the headers.

Params:

- **address**: Ethereum address

Examples:

- `ledger getTransactionCount 0x2fCbc96c55e57bed5ae8BC533EF77b977Dc482d7`

**`ledger getChainId`**

Command to get the chain id of the network. The JWT defined in `using oauth2token` is set in the headers.

Examples:

- `ledger getChainId`

**`ledger getTransaction <id>`**

Command to get a transaction by its ID. The JWT defined in `using oauth2token` is set in the headers.

Params:

- **id**: Transaction ID

Examples:

- `ledger getTransaction 0x2fCb...82d7`

**`ledger getTransactionReceipt <id>`**

Command to get the receipt of a particular transaction. It also displays the revert reason for failed transactions. The JWT defined in `using oauth2token` is set in the headers.

Params:

- **id**: Transaction ID

Examples:

- `ledger getTransactionReceipt 0x2fCb...482d7`

**`ledger sendTransaction <signedTransaction>`**

Command to send transactions to the blockchain. The JWT defined in `using oauth2token` is set in the headers.

Params:

- **signedTransaction**: signed transaction in raw format.

Examples:

- `ledger sendTransaction 0x2fCb...482d7 0x123456`

**`proxyledger <...>`**

The command `proxyledger` has the same commands as `ledger` with the difference that it will call the blockchain directly without going through Ledger API. To enable this feature set the corresponding provider in `TEST_BESU_PROVIDER` or `PILOT_BESU_PROVIDER` in the env variables. This command should not be used by externals.

Examples:

- `proxyledger timestamp timestampHashes`
- `proxyledger timestamp build-timestampHashes`
- `proxyledger tir insertIssuer did:ebsi:zkqR9GCLrLYbkubAjuqQZAz {"institution":"eu"}`
- `proxyledger getTransactionReceipt 0x2fCb...482d7`

## DID Registry API <a name="did-registry-api"></a>

OpenAPI documentation: https://api-pilot.ebsi.eu/docs/apis/did-registry/latest

**`did get <url>`**

Command to GET in the DID Registry API. All endpoints are open to the public.

Params:

- **url**: Path to get.

Examples:

- `did get /identifiers`
- `did get /identifiers?controller=0x2fCb...82d7`
- `did get /identifiers/did:ebsi:zkqR9GCLrLYbkubAjuqQZAz`

**`did (build-)insertDidDocument <did> <baseDocument> <publicKey> <notBefore> <notAfter>`**

Command to insert a new DID document. The JWT is set in the headers.

Use the `build-` prefix to call the API helper to build the transaction. If this prefix is not set then the command will perform the following steps:

- call `did build-insertDidDocument` to build an unsigned transaction.
- call `compute signTransaction` to sign the transaction with the user defined in `using user`.
- call `did sendSignedTransaction` to send the transaction signed.
- call `view transactionInfo` to see relevant data for this transaction.

Params:

- **did** (optional): DID to insert. By default it uses the did defined in the command `using user`.
- **baseDocument** (optional): JSON of the base document, that is, without controllers, verification methods, or verification relationships. By default it creates a JSON with a context.
- **publicKey** (optional): Public key for secp256k1 as hex string. This key will be inserted in the verification methods and capability invocations. By default it takes the ES256K from the user defined in `using user`.
- **notBefore** (optional): Initial time where the public key is valid. By default it takes the actual time.
- **notAfter** (optional): Time of expiration. By default it is 5 years.

Examples:

- `did insertDidDocument`
- `did insertDidDocument did:ebsi:zkqR9GCLrLYbkubAjuqQZAz`
- `did insertDidDocument did:ebsi:zkqR9GCLrLYbkubAjuqQZAz {"@context":"..."}`
- `did insertDidDocument did:ebsi:zkqR9GCLrLYbkubAjuqQZAz {"@context":"..."} 0x04375f4aa09a2555db1397cebac6b03312c48d658ee370d98598e1857e62c7121d6908019a91c51e90c1c7dc8ef3c4b9e3b03da1662ffe3d81f4ded664e4510285`

**`did (build-)updateBaseDocument <did> <baseDocument>`**

Command to update the base document of the DID document. The JWT is set in the headers.

Use the `build-` prefix to call the API helper to build the transaction. If this prefix is not set then the command will perform the following steps:

- call `did build-updateBaseDocument` to build an unsigned transaction.
- call `compute signTransaction` to sign the transaction with the user defined in `using user`.
- call `did sendSignedTransaction` to send the transaction signed.
- call `view transactionInfo` to see relevant data for this transaction.

Params:

- **did**: DID to update.
- **baseDocument** (optional): JSON of the base document, that is, without controllers, verification methods, or verification relationships. By default it creates a JSON with a context.

Examples:

- `did updateBaseDocument did:ebsi:zkqR9GCLrLYbkubAjuqQZAz {"@context":"..."}`

**`did (build-)addController <did> <controller>`**

Command to add a controller to the DID document. The JWT is set in the headers.

Use the `build-` prefix to call the API helper to build the transaction. If this prefix is not set then the command will perform the following steps:

- call `did build-addController` to build an unsigned transaction.
- call `compute signTransaction` to sign the transaction with the user defined in `using user`.
- call `did sendSignedTransaction` to send the transaction signed.
- call `view transactionInfo` to see relevant data for this transaction.

Params:

- **did**: DID to update.
- **controller**: DID of the new controller.

Examples:

- `did addController did:ebsi:zkqR9GCLrLYbkubAjuqQZAz did:ebsi:zosmFJJL3HydFsioVJnDLsz`

**`did (build-)revokeController <did> <controller>`**

Command to revoke a controller from the DID document. The JWT is set in the headers.

Use the `build-` prefix to call the API helper to build the transaction. If this prefix is not set then the command will perform the following steps:

- call `did build-revokeController` to build an unsigned transaction.
- call `compute signTransaction` to sign the transaction with the user defined in `using user`.
- call `did sendSignedTransaction` to send the transaction signed.
- call `view transactionInfo` to see relevant data for this transaction.

Params:

- **did**: DID to update.
- **controller**: DID of the new controller.

Examples:

- `did revokeController did:ebsi:zkqR9GCLrLYbkubAjuqQZAz did:ebsi:zosmFJJL3HydFsioVJnDLsz`

**`did (build-)addVerificationMethod <did> <publicKeyOrAlg>`**

Command to add a verification method to the DID document. The JWT is set in the headers.

Use the `build-` prefix to call the API helper to build the transaction. If this prefix is not set then the command will perform the following steps:

- call `did build-addVerificationMethod` to build an unsigned transaction.
- call `compute signTransaction` to sign the transaction with the user defined in `using user`.
- call `did sendSignedTransaction` to send the transaction signed.
- call `view transactionInfo` to see relevant data for this transaction.

Params:

- **did**: DID to update.
- **publicKeyOrAlg**: Public key of the verification method or algorithm. It can be an hex string for secp256k1 or a JWK for the rest of algorithms. If the algorithm is provided it will take the public key from the keys of the user.

Examples:

- `did addVerificationMethod did:ebsi:zkqR9GCLrLYbkubAjuqQZAz 0x04375f4aa09a2555db1397cebac6b03312c48d658ee370d98598e1857e62c7121d6908019a91c51e90c1c7dc8ef3c4b9e3b03da1662ffe3d81f4ded664e4510285`
- `did addVerificationMethod did:ebsi:zkqR9GCLrLYbkubAjuqQZAz {"kty":"EC","crv":"P-256","x":"6fx6xYsiw_1r-eiPd1qppd-T8jO5ciw-nGiiFWMlwic","y":"xZxJ2O0p6tW2QoRHZhZthgn5tUUMhHhHlS-2uprJr2w"}`
- `did addVerificationMethod did:ebsi:zkqR9GCLrLYbkubAjuqQZAz ES256K`

**`did (build-)addVerificationRelationship <did> <name> <vMethodIdOrAlg> <notBefore> <notAfter>`**

Command to add a verification relationship to the DID document. The JWT is set in the headers.

Use the `build-` prefix to call the API helper to build the transaction. If this prefix is not set then the command will perform the following steps:

- call `did build-addVerificationRelationship` to build an unsigned transaction.
- call `compute signTransaction` to sign the transaction with the user defined in `using user`.
- call `did sendSignedTransaction` to send the transaction signed.
- call `view transactionInfo` to see relevant data for this transaction.

Params:

- **did**: DID to update.
- **name**: Name of the verification relationship (authentication, assertionMethod, keyAgreement, capabilityInvocation, or capabilityDelegation)
- **vMethodIdOrAlg**: Reference to the verification method linked to this verification relationship. If the algorithm is provided it will take the verification method id from the keys of the user.
- **notBefore** (optional): Initial time where the relationship is valid. By default it takes the actual time.
- **notAfter** (optional): Time of expiration. By default it is 5 years.

Examples:

- `did addVerificationRelationship did:ebsi:zkqR9GCLrLYbkubAjuqQZAz assertionMethod eTYvGiq0uw7PPe0CwP3ePEK83DHNibu0lLs7y-7h2AQ`
- `did addVerificationRelationship did:ebsi:zkqR9GCLrLYbkubAjuqQZAz assertionMethod ES256K`
- `did addVerificationRelationship did:ebsi:zkqR9GCLrLYbkubAjuqQZAz assertionMethod eTYvGiq0uw7PPe0CwP3ePEK83DHNibu0lLs7y-7h2AQ 1669134481 1669150000`

**`did (build-)revokeVerificationMethod <did> <vMethodIdOrAlg> <notAfter>`**

Command to revoke a verification method in the DID document. The JWT is set in the headers.

Use the `build-` prefix to call the API helper to build the transaction. If this prefix is not set then the command will perform the following steps:

- call `did build-revokeVerificationMethod` to build an unsigned transaction.
- call `compute signTransaction` to sign the transaction with the user defined in `using user`.
- call `did sendSignedTransaction` to send the transaction signed.
- call `view transactionInfo` to see relevant data for this transaction.

Params:

- **did**: DID to update.
- **vMethodId**: Reference to the verification method linked to this verification relationship. If the algorithm is provided it will take the verification method id from the keys of the user.
- **notAfter** (optional): Time of expiration. By default it is the actual time.

Examples:

- `did revokeVerificationMethod did:ebsi:zkqR9GCLrLYbkubAjuqQZAz eTYvGiq0uw7PPe0CwP3ePEK83DHNibu0lLs7y-7h2AQ`
- `did revokeVerificationMethod did:ebsi:zkqR9GCLrLYbkubAjuqQZAz ES256K`
- `did revokeVerificationMethod did:ebsi:zkqR9GCLrLYbkubAjuqQZAz eTYvGiq0uw7PPe0CwP3ePEK83DHNibu0lLs7y-7h2AQ 1669134481`

**`did (build-)expireVerificationMethod <did> <vMethodIdOrAlg> <notAfter>`**

Command to set an expiration time for a verification method in the DID document. The JWT is set in the headers.

Use the `build-` prefix to call the API helper to build the transaction. If this prefix is not set then the command will perform the following steps:

- call `did build-expireVerificationMethod` to build an unsigned transaction.
- call `compute signTransaction` to sign the transaction with the user defined in `using user`.
- call `did sendSignedTransaction` to send the transaction signed.
- call `view transactionInfo` to see relevant data for this transaction.

Params:

- **did**: DID to update.
- **vMethodIdOrAlg**: Reference to the verification method linked to this verification relationship. If the algorithm is provided it will take the verification method id from the keys of the user.
- **notAfter**: Time of expiration.

Examples:

- `did expireVerificationMethod did:ebsi:zkqR9GCLrLYbkubAjuqQZAz eTYvGiq0uw7PPe0CwP3ePEK83DHNibu0lLs7y-7h2AQ`
- `did expireVerificationMethod did:ebsi:zkqR9GCLrLYbkubAjuqQZAz ES256K`
- `did expireVerificationMethod did:ebsi:zkqR9GCLrLYbkubAjuqQZAz eTYvGiq0uw7PPe0CwP3ePEK83DHNibu0lLs7y-7h2AQ 1669134481`

**`did (build-)rollVerificationMethod <did> <publicKeyOrAlg> <notBefore> <notAfter> <oldVMethodId> <duration>`**

Command to roll a verification method in the DID document. The JWT is set in the headers.

Use the `build-` prefix to call the API helper to build the transaction. If this prefix is not set then the command will perform the following steps:

- call `did build-rollVerificationMethod` to build an unsigned transaction.
- call `compute signTransaction` to sign the transaction with the user defined in `using user`.
- call `did sendSignedTransaction` to send the transaction signed.
- call `view transactionInfo` to see relevant data for this transaction.

Params:

- **did**: DID to update.
- **publicKeyOrAlg**: Public key of the new verification method. It can be an hex string for secp256k1 or a JWK for the rest of algorithms. If the algorithm is provided it will take the public key from the keys of the user.
- **notBefore**: Initial time where the public key is valid.
- **notAfter**: Time of expiration.
- **oldVMethodId**: Reference to the verification method that will be rolled.
- **duration**: Transition period where both verification methods are valid.

Examples:

- `did rollVerificationMethod did:ebsi:zkqR9GCLrLYbkubAjuqQZAz 0x04375f4aa09a2555db1397cebac6b03312c48d658ee370d98598e1857e62c7121d6908019a91c51e90c1c7dc8ef3c4b9e3b03da1662ffe3d81f4ded664e4510285 2024 2030 eTYvGiq0uw7PPe0CwP3ePEK83DHNibu0lLs7y-7h2AQ 3600`
- `did rollVerificationMethod did:ebsi:zkqR9GCLrLYbkubAjuqQZAz ES256K 2024 2030 eTYvGiq0uw7PPe0CwP3ePEK83DHNibu0lLs7y-7h2AQ 3600`

**`did sendSignedTransaction <unsignedTransaction> <signedTransaction>`**

Command to send a signed transaction to the DID Registry. The JWT is set in the headers. It's also possible to load the parameters from the saved variables by referencing the names.

Params:

- **unsignedTransaction**: JSON with the unsigned transaction.
- **signedTransaction**: Signed transaction in raw format.

Examples:

- `did sendSignedTransaction {"from":"0x6b...} 0xf9022...`
- `did sendSignedTransaction utx sgntx`

## Timestamp API <a name="timestamp-api"></a>

OpenAPI documentation: https://api-pilot.ebsi.eu/docs/apis/timestamp/latest

**`timestamp get <url>`**

Command to GET in the Timestamp API. All endpoints are open to the public.

Params:

- **url**: Path to get.

Examples:

- `timestamp get /timestamps`
- `timestamp get /timestamps/uMHh...`
- `timestamp get /hash-algorithms`
- `timestamp get /records`

**`timestamp (build-)insertHashAlgorithm <outputLength> <ianaName> <oid> <status> <multihash>`**

Command to insert a new hash algorithm. The JWT is set in the headers. This method requires admin rights in the Trusted Apps Registry.

Use the `build-` prefix to call the API helper to build the transaction. If this prefix is not set then the command will perform the following steps:

- call `timestamp build-insertHashAlgorithm` to build an unsigned transaction.
- call `compute signTransaction` to sign the transaction with the user defined in `using user`.
- call `timestamp sendSignedTransaction` to send the transaction signed.
- call `view transactionInfo` to see relevant data for this transaction.

Params:

- **outputLength** (optional): JSON data to store. By default it is 1.
- **ianaName** (optional): IANA name. By default it is "undefined".
- **oid** (optional): OID. By default it is "undefined".
- **status** (optional): Status. It must be 1 (default) or 2 which corresponds with active or revoked respectively.
- **multihash** (optional): Multihash identifier. By default it uses de ianaName.

Examples:

- `timestamp insertHashAlgorithm 256 sha-256 2.16.840.1.101.3.4.2.1 1 sha2-256`
- `timestamp insertHashAlgorithm 512 sha3-512 2.16.840.1.101.3.4.2.10 1 sha3-512`

**`timestamp (build-)updateHashAlgorithm <hashAlgId> <outputLength> <ianaName> <oid> <status> <multihash>`**

Command to update an existing hash algorithm. The JWT is set in the headers. This method requires admin rights in the Trusted Apps Registry.

Use the `build-` prefix to call the API helper to build the transaction. If this prefix is not set then the command will perform the following steps:

- call `timestamp build-updateHashAlgorithm` to build an unsigned transaction.
- call `compute signTransaction` to sign the transaction with the user defined in `using user`.
- call `timestamp sendSignedTransaction` to send the transaction signed.
- call `view transactionInfo` to see relevant data for this transaction.

Params:

- **hashAlgId**: Hash Algorithm ID to update.
- **outputLength** (optional): JSON data to store. By default it is 1.
- **ianaName** (optional): IANA name. By default it is "undefined".
- **oid** (optional): OID. By default it is "undefined".
- **status** (optional): Status. It must be 1 (default) or 2 which corresponds with active or revoked respectively.
- **multihash** (optional): Multihash identifier. By default it uses de ianaName.

Examples:

- `timestamp updateHashAlgorithm 1 256 sha-256 2.16.840.1.101.3.4.2.1 1 sha2-256`
- `timestamp updateHashAlgorithm 7 512 sha3-512 2.16.840.1.101.3.4.2.10 1 sha3-512`

**`timestamp (build-)timestampRecordHashes <data> <info>`**

Command to timestamp data and create a record of it with some info. The JWT is set in the headers.

Use the `build-` prefix to call the API helper to build the transaction. If this prefix is not set then the command will perform the following steps:

- call `timestamp build-timestampRecordHashes` to build an unsigned transaction.
- call `compute signTransaction` to sign the transaction with the user defined in `using user`.
- call `timestamp sendSignedTransaction` to send the transaction signed.
- call `view transactionInfo` to see relevant data for this transaction.

Params:

- **data** (optional): JSON data to timestamp. By default it creates random data.
- **info** (optional): JSON info to insert in the record. By default it creates random data.

Examples:

- `timestamp timestampRecordHashes`
- `timestamp timestampRecordHashes {"data":"my data"} {"info":"test"}`

**`timestamp (build-)timestampVersionHashes <previousHash> <data> <info>`**

Command to create a new version of an existing record given the previous version hash. This new version is timestamped. Use it when you don't have the recordId but you have the hash that was timestamped in the previous version. You can not use this function if several records share the same hash, for such cases use `timestampRecordVersionHashes`. The JWT is set in the headers. Only record owners are allowed to perform this operation.

Use the `build-` prefix to call the API helper to build the transaction. If this prefix is not set then the command will perform the following steps:

- call `timestamp build-timestampVersionHashes` to build an unsigned transaction.
- call `compute signTransaction` to sign the transaction with the user defined in `using user`.
- call `timestamp sendSignedTransaction` to send the transaction signed.
- call `view transactionInfo` to see relevant data for this transaction.

Params:

- **previousHash**: Hash timestamped in a previous version
- **data** (optional): JSON data to timestamp. By default it creates random data.
- **info** (optional): JSON info to insert in the new version of the record. By default it creates random data.

Examples:

- `timestamp timestampVersionHashes 0x12f5...`
- `timestamp timestampVersionHashes 0x12f5... {"data":"my data"} {"info":"test"}`

**`timestamp (build-)timestampRecordVersionHashes <recordId> <data> <info>`**

Command to create a new version of an existing record. This new version is timestamped. The JWT is set in the headers. Only record owners are allowed to perform this operation.

Use the `build-` prefix to call the API helper to build the transaction. If this prefix is not set then the command will perform the following steps:

- call `timestamp build-timestampRecordVersionHashes` to build an unsigned transaction.
- call `compute signTransaction` to sign the transaction with the user defined in `using user`.
- call `timestamp sendSignedTransaction` to send the transaction signed.
- call `view transactionInfo` to see relevant data for this transaction.

Params:

- **recordId**: Record ID in hex string format
- **data** (optional): JSON data to timestamp. By default it creates random data.
- **info** (optional): JSON info to insert in the new version of the record. By default it creates random data.

Examples:

- `timestamp timestampRecordVersionHashes 0x12f5...`
- `timestamp timestampRecordVersionHashes 0x12f5... {"data":"my data"} {"info":"test"}`

**`timestamp (build-)insertRecordOwner <recordId> <ownerId> <notBefore> <notAfter>`**

Command to insert a new owner in an existing record. The JWT is set in the headers. Only record owners are allowed to perform this operation.

Use the `build-` prefix to call the API helper to build the transaction. If this prefix is not set then the command will perform the following steps:

- call `timestamp build-insertRecordOwner` to build an unsigned transaction.
- call `compute signTransaction` to sign the transaction with the user defined in `using user`.
- call `timestamp sendSignedTransaction` to send the transaction signed.
- call `view transactionInfo` to see relevant data for this transaction.

Params:

- **recordId**: Record ID in hex string format
- **ownerId**: Ethereum address of new owner.
- **notBefore** (optional): Point in time when the owner becomes valid. It should be defined in epoch time. By default it takes the actual time.
- **notAfter** (optional): Point in time when the owner becomes invalid. It should be defined in epoch time. By default it is 0 (indefinite).

Examples:

- `timestamp insertRecordOwner 0x12f5... 0xab54...`
- `timestamp insertRecordOwner 0x12f5... 0xab54... 1625568669 1657104669`

**`timestamp (build-)revokeRecordOwner <recordId> <ownerId>`**

Command to revoke new owner from an existing record and put it in the revoked list. The JWT is set in the headers. Only record owners are allowed to perform this operation.

Use the `build-` prefix to call the API helper to build the transaction. If this prefix is not set then the command will perform the following steps:

- call `timestamp build-revokeRecordOwner` to build an unsigned transaction.
- call `compute signTransaction` to sign the transaction with the user defined in `using user`.
- call `timestamp sendSignedTransaction` to send the transaction signed.
- call `view transactionInfo` to see relevant data for this transaction.

Params:

- **recordId**: Record ID in hex string format.
- **ownerId**: Ethereum address of the owner to revoke.

Examples:

- `timestamp revokeRecordOwner 0x12f5... 0xab54...`

**`timestamp (build-)insertRecordVersionInfo <recordId> <versionId> <info>`**

Command to insert new info to a version of an existing record. The JWT is set in the headers. Only record owners are allowed to perform this operation.

Use the `build-` prefix to call the API helper to build the transaction. If this prefix is not set then the command will perform the following steps:

- call `timestamp build-insertRecordVersionInfo` to build an unsigned transaction.
- call `compute signTransaction` to sign the transaction with the user defined in `using user`.
- call `timestamp sendSignedTransaction` to send the transaction signed.
- call `view transactionInfo` to see relevant data for this transaction.

Params:

- **recordId**: Record ID in hex string format.
- **versionId**: Number of the version.
- **info** (optional): JSON info to insert in the version.

Examples:

- `timestamp insertRecordVersionInfo 0x12f5... 1`
- `timestamp insertRecordVersionInfo 0x12f5... 1 {"info":"test"}`

**`timestamp (build-)detachRecordVersionHash <recordId> <versionId> <hash>`**

Command to remove a hash from a version of an existing record. The JWT is set in the headers. Only record owners are allowed to perform this operation.

Use the `build-` prefix to call the API helper to build the transaction. If this prefix is not set then the command will perform the following steps:

- call `timestamp build-detachRecordVersionHash` to build an unsigned transaction.
- call `compute signTransaction` to sign the transaction with the user defined in `using user`.
- call `timestamp sendSignedTransaction` to send the transaction signed.
- call `view transactionInfo` to see relevant data for this transaction.

Params:

- **recordId**: Record ID in hex string format.
- **versionId**: Number of the version.
- **hash**: Hash of timestamp to detach.

Examples:

- `timestamp detachRecordVersionHash 0x12f5... 1 0xfd45...`

**`timestamp (build-)appendRecordVersionHashes <recordId> <versionId> <data> <info>`**

Command to include more hashes to an existing version. For instance, use it if you want to have different hashes (sha2-256, sha3-512, etc) of the same data. The JWT is set in the headers. Only record owners are allowed to perform this operation.

_DISCLAIMER: In the current version the CLI the "data" is hashed with sha2-256. Future versions will include more algorithms._

Use the `build-` prefix to call the API helper to build the transaction. If this prefix is not set then the command will perform the following steps:

- call `timestamp build-appendRecordVersionHashes` to build an unsigned transaction.
- call `compute signTransaction` to sign the transaction with the user defined in `using user`.
- call `timestamp sendSignedTransaction` to send the transaction signed.
- call `view transactionInfo` to see relevant data for this transaction.

Params:

- **recordId**: Record ID in hex string format
- **versionId**: Number of the version.
- **data** (optional): JSON data to timestamp. By default it creates random data.
- **info** (optional): JSON info to append in the version of the record. By default it creates random data.

Examples:

- `timestamp appendRecordVersionHashes 0x12f5... 1`
- `timestamp appendRecordVersionHashes 0x12f5... 1 {"data":"my data"} {"info":"test"}`

**`timestamp (build-)timestampHashes <data>`**

Command to timestamp data. The JWT is set in the headers.

Use the `build-` prefix to call the API helper to build the transaction. If this prefix is not set then the command will perform the following steps:

- call `timestamp build-timestampHashes` to build an unsigned transaction.
- call `compute signTransaction` to sign the transaction with the user defined in `using user`.
- call `timestamp sendSignedTransaction` to send the transaction signed.
- call `view transactionInfo` to see relevant data for this transaction.

Params:

- **data** (optional): JSON data to timestamp. By default it creates random data.

Examples:

- `timestamp timestampHashes`
- `timestamp timestampHashes {"data":"my data"}`

**`timestamp sendSignedTransaction <unsignedTransaction> <signedTransaction>`**

Command to send a signed transaction to the Timestamp Registry. The JWT is set in the headers. It's also possible to load the parameters from the saved variables by referencing the names.

Params:

- **unsignedTransaction**: JSON with the unsigned transaction.
- **signedTransaction**: Signed transaction in raw format.

Examples:

- `timestamp sendSignedTransaction {"from":"0x6b...} 0xf9022...`
- `timestamp sendSignedTransaction utx sgntx`

## Trusted Apps Registry API <a name="trusted-apps-registry-api"></a>

OpenAPI documentation: https://api-pilot.ebsi.eu/docs/apis/trusted-apps-registry/latest

**`tar get <url>`**

Command to GET in the Trusted Apps Registry API. All endpoints are open to the public.

Params:

- **url**: Path to get.

Examples:

- `tar get /apps`
- `tar get /apps?public_key_id=0x3f...`
- `tar get /apps/ledger-api/authorizations?requesterApplicationId=0x45...`
- `tar get /policies`

**`tar (build-)insertPolicy <name> <data>`**

Command to insert a policy. The JWT is set in the headers. This method requires admin rights in the Trusted Apps Registry.

Use the `build-` prefix to call the API helper to build the transaction. If this prefix is not set then the command will perform the following steps:

- call `tar build-insertPolicy` to build an unsigned transaction.
- call `compute signTransaction` to sign the transaction with the user defined in `using user`.
- call `tar sendSignedTransaction` to send the transaction signed.
- call `view transactionInfo` to see relevant data for this transaction.

Params:

- **name** (optional): Name of the policy. By default it uses a random name.
- **data** (optional): JSON data to store in the policy. By default it uses random data.

Examples:

- `tar insertPolicy`
- `tar insertPolicy my-policy {"name":"my policy"}`

**`tar (build-)updatePolicy <name> <data>`**

Command to update an existing policy. The JWT is set in the headers. This method requires admin rights in the Trusted Apps Registry.

Use the `build-` prefix to call the API helper to build the transaction. If this prefix is not set then the command will perform the following steps:

- call `tar build-updatePolicy` to build an unsigned transaction.
- call `compute signTransaction` to sign the transaction with the user defined in `using user`.
- call `tar sendSignedTransaction` to send the transaction signed.
- call `view transactionInfo` to see relevant data for this transaction.

Params:

- **name**: Name of the policy.
- **data** (optional): JSON data to store in the policy. By default it uses random data.

Examples:

- `tar updatePolicy my-policy {"name":"my policy"}`

**`tar (build-)insertAuthorization <appName> <authorizedAppName> <issuer> <status> <permissions> <notBefore> <notAfter>`**

Command to insert a new authorization. The JWT is set in the headers. This method requires admin rights in the Trusted Apps Registry.

Use the `build-` prefix to call the API helper to build the transaction. If this prefix is not set then the command will perform the following steps:

- call `tar build-insertAuthorization` to build an unsigned transaction.
- call `compute signTransaction` to sign the transaction with the user defined in `using user`.
- call `tar sendSignedTransaction` to send the transaction signed.
- call `view transactionInfo` to see relevant data for this transaction.

Params:

- **appName**: Resource application name.
- **authorizedAppName**: Application that will have access to the resource application.
- **issuer** (optional): DID of the issuer. By default it takes the DID defined in `using user`.
- **status** (optional): Status. It must be 1 (default), 2, or 3 which corresponds with active, revoked, or suspended respectively.
- **permissions** (optional): CRUD permissions. It must be a number between 0 and 15 (default). It's binary representation enables or disables each one of the 4 CRUD permissions (create, read, update, delete).
- **notBefore** (optional): Point in time when the authorization becomes valid. It should be defined in epoch time. By default it takes the actual time.
- **notAfter** (optional): Point in time when the authorization becomes invalid. It should be defined in epoch time. By default it is 0 (indefinite).

Examples:

- `tar insertAuthorization ledger-api my-app`
- `tar insertAuthorization ledger-api my-app did:ebsi:zkqR9GCLrLYbkubAjuqQZAz 1 15 1625568669 1657104669`

**`tar (build-)updateAuthorization <id> <status> <permissions> <notAfter>`**

Command to update an authorization. The JWT is set in the headers. This method requires admin rights in the Trusted Apps Registry.

Use the `build-` prefix to call the API helper to build the transaction. If this prefix is not set then the command will perform the following steps:

- call `tar build-updateAuthorization` to build an unsigned transaction.
- call `compute signTransaction` to sign the transaction with the user defined in `using user`.
- call `tar sendSignedTransaction` to send the transaction signed.
- call `view transactionInfo` to see relevant data for this transaction.

Params:

- **id**: Authorization ID.
- **status** (optional): Status. It must be 1 (default), 2, or 3 which corresponds with active, revoked, or suspended respectively.
- **permissions** (optional): CRUD permissions. It must be a number between 0 and 15 (default). It's binary representation enables or disables each one of the 4 CRUD permissions (create, read, update, delete).
- **notAfter** (optional): Point in time when the did method becomes invalid. It should be defined in epoch time. By default it is 0 (indefinite).

Examples:

- `tar updateAuthorization 0x12...4f`
- `tar updateAuthorization 0x12...4f 1 15 1657104669`

**`tar (build-)insertApp <name> <appAdministrator>`**

Command to insert a new app in the Trusted Apps Registry. The JWT is set in the headers. This method requires admin rights in the Trusted Apps Registry.

Use the `build-` prefix to call the API helper to build the transaction. If this prefix is not set then the command will perform the following steps:

- call `tar build-insertApp` to build an unsigned transaction.
- call `compute signTransaction` to sign the transaction with the user defined in `using user`.
- call `tar sendSignedTransaction` to send the transaction signed.
- call `view transactionInfo` to see relevant data for this transaction.

Params:

- **name**: Name of the new app.
- **appAdministrator** (optional): Did of app administrator. By default it takes the did defined in the command `using user`.

Examples:

- `tar insertApp my-app did:ebsi:znbuGDt6tEqpGZNAuGc2uvZ`

**`tar (build-)updateApp <id> <name> <domain>`**

Command to update an existing app. The JWT is set in the headers. This method requires admin rights in the Trusted Apps Registry.

Use the `build-` prefix to call the API helper to build the transaction. If this prefix is not set then the command will perform the following steps:

- call `tar build-updateApp` to build an unsigned transaction.
- call `compute signTransaction` to sign the transaction with the user defined in `using user`.
- call `tar sendSignedTransaction` to send the transaction signed.
- call `view transactionInfo` to see relevant data for this transaction.

Params:

- **id**: Application ID
- **name**: New name.
- **domain** (optional): New domain. It must be 1 (default) or 2, which corresponds with "ebsi" and "external" respectively.

Examples:

- `tar updateApp 0x123...fe5 new-name`
- `tar updateApp 0x123...fe5 new-name 2`

**`tar (build-)insertAppInfo <id> <info>`**

Command to insert info in an existing app. The JWT is set in the headers. This method requires admin rights in the Trusted Apps Registry.

Use the `build-` prefix to call the API helper to build the transaction. If this prefix is not set then the command will perform the following steps:

- call `tar build-insertAppInfo` to build an unsigned transaction.
- call `compute signTransaction` to sign the transaction with the user defined in `using user`.
- call `tar sendSignedTransaction` to send the transaction signed.
- call `view transactionInfo` to see relevant data for this transaction.

Params:

- **id**: Application ID
- **info**: JSON info.

Examples:

- `tar insertAppInfo 0x123...fe5 {"name":"my app"}`

**`tar (build-)insertAppPublicKey <id> <publicKey> <status> <notBefore> <notAfter>`**

Command to insert a new public key in an app. The JWT is set in the headers. This method requires admin rights in the Trusted Apps Registry.

Use the `build-` prefix to call the API helper to build the transaction. If this prefix is not set then the command will perform the following steps:

- call `tar build-insertAppPublicKey` to build an unsigned transaction.
- call `compute signTransaction` to sign the transaction with the user defined in `using user`.
- call `tar sendSignedTransaction` to send the transaction signed.
- call `view transactionInfo` to see relevant data for this transaction.

Params:

- **id**: Application ID
- **publicKey**: Public key. Insert base64 representation of PEM string.
- **status** (optional): Status. It must be 1 (default), 2, or 3 which corresponds with active, revoked, or suspended respectively.
- **notBefore** (optional): Point in time when the public key becomes valid. It should be defined in epoch time. By default it takes the actual time.
- **notAfter** (optional): Point in time when the public key becomes invalid. It should be defined in epoch time. By default it is 0 (indefinite).

Examples:

- `tar insertAppPublicKey LS0tLS1...`
- `tar insertAppPublicKey LS0tLS1... 1 1625568669 1657104669`

**`tar (build-)updateAppPublicKey <id> <status> <notAfter>`**

Command to update an existing public key. The JWT is set in the headers. This method requires admin rights in the Trusted Apps Registry.

Use the `build-` prefix to call the API helper to build the transaction. If this prefix is not set then the command will perform the following steps:

- call `tar build-updateAppPublicKey` to build an unsigned transaction.
- call `compute signTransaction` to sign the transaction with the user defined in `using user`.
- call `tar sendSignedTransaction` to send the transaction signed.
- call `view transactionInfo` to see relevant data for this transaction.

Params:

- **id**: Public key ID
- **status** (optional): Status. It must be 1 (default), 2, or 3 which corresponds with active, revoked, or suspended respectively.
- **notAfter** (optional): Point in time when the public key becomes invalid. It should be defined in epoch time. By default it is 0 (indefinite).

Examples:

- `tar updateAppPublicKey 0x45...f1`
- `tar updateAppPublicKey 0x45...f1 1 1657104669`

**`tar (build-)insertAppAdministrator <id> <didAdmin>`**

Command to insert an app administrator to an existing app. The JWT is set in the headers. This method requires admin rights in the Trusted Apps Registry.

Use the `build-` prefix to call the API helper to build the transaction. If this prefix is not set then the command will perform the following steps:

- call `tar build-insertAppAdministrator` to build an unsigned transaction.
- call `compute signTransaction` to sign the transaction with the user defined in `using user`.
- call `tar sendSignedTransaction` to send the transaction signed.
- call `view transactionInfo` to see relevant data for this transaction.

Params:

- **id**: Application ID.
- **didAdmin**: DID of new app admin.

Examples:

- `tar insertAppAdministrator 0x45...f1 did:ebsi:zkqR9GCLrLYbkubAjuqQZAz`

**`tar (build-)deleteAppAdministrator <id> <didAdmin>`**

Command to delete an app administrator from an existing app. The JWT is set in the headers. This method requires admin rights in the Trusted Apps Registry.

Use the `build-` prefix to call the API helper to build the transaction. If this prefix is not set then the command will perform the following steps:

- call `tar build-deleteAppAdministrator` to build an unsigned transaction.
- call `compute signTransaction` to sign the transaction with the user defined in `using user`.
- call `tar sendSignedTransaction` to send the transaction signed.
- call `view transactionInfo` to see relevant data for this transaction.

Params:

- **id**: Application ID.
- **didAdmin**: DID to delete from app admins.

Examples:

- `tar deleteAppAdministrator 0x45...f1 did:ebsi:zkqR9GCLrLYbkubAjuqQZAz`

**`tar (build-)insertRevocation <id> <revokedBy> <notBefore>`**

Command to revoke an app. It will be moved to the revocation list. The JWT is set in the headers. This method requires admin rights in the Trusted Apps Registry.

Use the `build-` prefix to call the API helper to build the transaction. If this prefix is not set then the command will perform the following steps:

- call `tar build-insertRevocation` to build an unsigned transaction.
- call `compute signTransaction` to sign the transaction with the user defined in `using user`.
- call `tar sendSignedTransaction` to send the transaction signed.
- call `view transactionInfo` to see relevant data for this transaction.

Params:

- **id**: Application ID.
- **revokedBy**: DID of the admin performing the revocation.
- **notBefore** (optional): Point in time when the app becomes revoked. It should be defined in epoch time. By default it takes the actual time.

Examples:

- `tar insertRevocation 0x45...f1 did:ebsi:zkqR9GCLrLYbkubAjuqQZAz`
- `tar insertRevocation 0x45...f1 did:ebsi:zkqR9GCLrLYbkubAjuqQZAz 1625568669`

**`tar sendSignedTransaction <unsignedTransaction> <signedTransaction>`**

Command to send a signed transaction to the Trusted Apps Registry. The JWT is set in the headers. It's also possible to load the parameters from the saved variables by referencing the names.

Params:

- **unsignedTransaction**: JSON with the unsigned transaction.
- **signedTransaction**: Signed transaction in raw format.

Examples:

- `tar sendSignedTransaction {"from":"0x6b...} 0xf9022...`
- `tar sendSignedTransaction utx sgntx`

## Trusted Issuers Registry API <a name="trusted-issuers-registry-api"></a>

OpenAPI documentation: https://api-pilot.ebsi.eu/docs/apis/trusted-issuers-registry/latest

**`tir get <url>`**

Command to GET in the Trusted Issuers Registry API.

Params:

- **url**: Path to get.

Examples:

- `tir get /issuers`
- `tir get /issuers/did:ebsi:12a...`
- `tir get /policies`

**`tir (build-)insertPolicy <name> <data>`**

Command to insert a policy. The JWT is set in the headers. This method requires admin rights in the Trusted Issuers Registry.

Use the `build-` prefix to call the API helper to build the transaction. If this prefix is not set then the command will perform the following steps:

- call `tir build-insertPolicy` to build an unsigned transaction.
- call `compute signTransaction` to sign the transaction with the user defined in `using user`.
- call `tir sendSignedTransaction` to send the transaction signed.
- call `view transactionInfo` to see relevant data for this transaction.

Params:

- **name** (optional): Name of the policy. By default it uses a random name.
- **data** (optional): JSON data to store in the policy. By default it uses random data.

Examples:

- `tir insertPolicy`
- `tir insertPolicy my-policy {"name":"my policy"}`

**`tir (build-)updatePolicy <name> <data>`**

Command to update an existing policy. The JWT is set in the headers. This method requires admin rights in the Trusted Issuers Registry.

Use the `build-` prefix to call the API helper to build the transaction. If this prefix is not set then the command will perform the following steps:

- call `tir build-updatePolicy` to build an unsigned transaction.
- call `compute signTransaction` to sign the transaction with the user defined in `using user`.
- call `tir sendSignedTransaction` to send the transaction signed.
- call `view transactionInfo` to see relevant data for this transaction.

Params:

- **name**: Name of the policy.
- **data** (optional): JSON data to store in the policy. By default it uses random data.

Examples:

- `tir updatePolicy my-policy {"name":"my policy"}`

**`tir (build-)insertIssuer <did> <attribute> <issuerType> <taoDid> <taoAttributeId>`**

Command to insert a new issuer in the registry. The JWT is set in the headers. Users with admin rights in the Trusted Policies Registry can insert RootTAOs. RootTAOs can insert new TAOs in the same trust chain. And TAOs can insert issuers.

Use the `build-` prefix to call the API helper to build the transaction. If this prefix is not set then the command will perform the following steps:

- call `tir build-insertIssuer` to build an unsigned transaction.
- call `compute signTransaction` to sign the transaction with the user defined in `using user`.
- call `tir sendSignedTransaction` to send the transaction signed.
- call `view transactionInfo` to see relevant data for this transaction.

Params:

- **did**: DID of the new issuer.
- **attribute**: VC JWT, JSON attribute or buffer expressed as hex string.
- **issuerType**: roottao, tao, ti, or revoked.
- **taoDid** (optional): DID of the TAO accrediting the issuer. It should be defined when issuer type is different to roottao.
- **taoAttributeId** (optional): Attribute ID that accredit `taoDid` as TAO. It should be defined when issuer type is different to roottao.

Examples:

- `tir insertIssuer did:ebsi:zkqR9GCLrLYbkubAjuqQZAz ey... roottao`
- `tir insertIssuer did:ebsi:zkqR9GCLrLYbkubAjuqQZAz ey... tao did:ebsi:zpXNF1yMfRBND6cyaK4sfjJ 0x32660b7624ebf3e6a37c58804c9fa7a20304d080c069d12e5f7d8374e9a6051d`

**`tir (build-)updateIssuer <did> <attribute> <issuerType> <taoDid> <taoAttributeId> <prevAttributeHash>`**

Command to update the attribute to an existing issuer. The JWT is set in the headers. This update can be done by the corresponding TAO or RootTAO in the trust chain, or by an user with admin rights in the Trusted Policies Registry.

Use the `build-` prefix to call the API helper to build the transaction. If this prefix is not set then the command will perform the following steps:

- call `tir build-updateIssuer` to build an unsigned transaction.
- call `compute signTransaction` to sign the transaction with the user defined in `using user`.
- call `tir sendSignedTransaction` to send the transaction signed.
- call `view transactionInfo` to see relevant data for this transaction.

Params:

- **did**: DID of the issuer.
- **attribute**: VC JWT, JSON attribute or buffer expressed as hex string.
- **issuerType**: roottao, tao, ti, or revoked.
- **taoDid**: DID of the TAO accrediting the issuer (or itself in case of roottao).
- **taoAttributeId**: Attribute ID that accredit `taoDid` as TAO.
- **prevAttributeHash** (optional): Hash of the previous attribute. If it's not set a new attribute will be added.

Examples:

- `tir updateIssuer did:ebsi:zkqR9GCLrLYbkubAjuqQZAz ey... tao did:ebsi:zpXNF1yMfRBND6cyaK4sfjJ 0x32660b7624ebf3e6a37c58804c9fa7a20304d080c069d12e5f7d8374e9a6051d`

**`tir (build-)addIssuerProxy <did> <proxyData>`**

Command to add a proxy to an existing user. The JWT is set in the headers. This operation must be done by issuer itself or by an user with admin rights in the Trusted Policies Registry.

Use the `build-` prefix to call the API helper to build the transaction. If this prefix is not set then the command will perform the following steps:

- call `tir build-addIssuerProxy` to build an unsigned transaction.
- call `compute signTransaction` to sign the transaction with the user defined in `using user`.
- call `tir sendSignedTransaction` to send the transaction signed.
- call `view transactionInfo` to see relevant data for this transaction.

Params:

- **did**: DID of the issuer.
- **proxyData**: Proxy configuration to be used during the verification of credentials. For the registration to be accepted, the configured proxy must serve a status list which is valid and signed by the issuer requesting the proxy. The validation is done by combining "prefix" with "testSuffix" and executed before accepting the registration. Proxy must be configured before any VCs are issued. The validation will fail if the credential status proxy doesn't exist.

Examples:

- `tir addIssuerProxy did:ebsi:zkqR9GCLrLYbkubAjuqQZAz {"prefix":"https://example.net","headers":{"Authorization":"Bearer 1234567890"},"testSuffix":"/cred/1"}`

**`tir (build-)updateIssuerProxy <did> <proxyId> <proxyData>`**

Command to update a proxy of an existing user. The JWT is set in the headers. This operation must be done by issuer itself or by an user with admin rights in the Trusted Policies Registry.

Use the `build-` prefix to call the API helper to build the transaction. If this prefix is not set then the command will perform the following steps:

- call `tir build-updateIssuerProxy` to build an unsigned transaction.
- call `compute signTransaction` to sign the transaction with the user defined in `using user`.
- call `tir sendSignedTransaction` to send the transaction signed.
- call `view transactionInfo` to see relevant data for this transaction.

Params:

- **did**: DID of the issuer.
- **proxyId**: ID of the proxy to update.
- **proxyData**: Proxy configuration to be used during the verification of credentials. For the registration to be accepted, the configured proxy must serve a status list which is valid and signed by the issuer requesting the proxy. The validation is done by combining "prefix" with "testSuffix" and executed before accepting the registration. Proxy must be configured before any VCs are issued. The validation will fail if the credential status proxy doesn't exist.

Examples:

- `tir updateIssuerProxy 0x32660b7624ebf3e6a37c58804c9fa7a20304d080c069d12e5f7d8374e9a6051d did:ebsi:zkqR9GCLrLYbkubAjuqQZAz {"prefix":"https://example.net","headers":{"Authorization":"Bearer 1234567890"},"testSuffix":"/cred/1"}`

**`tir sendSignedTransaction <unsignedTransaction> <signedTransaction>`**

Command to send a signed transaction to the Trusted Issuers Registry. The JWT is set in the headers. It's also possible to load the parameters from the saved variables by referencing the names.

Params:

- **unsignedTransaction**: JSON with the unsigned transaction.
- **signedTransaction**: Signed transaction in raw format.

Examples:

- `tir sendSignedTransaction {"from":"0x6b...} 0xf9022...`
- `tir sendSignedTransaction utx sgntx`

## Trusted Schemas Registry API <a name="trusted-schemas-registry-api"></a>

OpenAPI documentation: https://api-pilot.ebsi.eu/docs/apis/trusted-schemas-registry/latest

**`tsr get <url>`**

Command to GET in the Trusted Schemas Registry API.

Params:

- **url**: Path to get.

Examples:

- `tsr get /schemas`
- `tsr get /schemas/0x12f...`
- `tsr get /policies`

**`tsr (build-)insertPolicy <name> <data>`**

Command to insert a policy. The JWT is set in the headers. This method requires admin rights in the Trusted Schemas Registry.

Use the `build-` prefix to call the API helper to build the transaction. If this prefix is not set then the command will perform the following steps:

- call `tsr build-insertPolicy` to build an unsigned transaction.
- call `compute signTransaction` to sign the transaction with the user defined in `using user`.
- call `tsr sendSignedTransaction` to send the transaction signed.
- call `view transactionInfo` to see relevant data for this transaction.

Params:

- **name** (optional): Name of the policy. By default it uses a random name.
- **data** (optional): JSON data to store in the policy. By default it uses random data.

Examples:

- `tsr insertPolicy`
- `tsr insertPolicy my-policy {"name":"my policy"}`

**`tsr (build-)updatePolicy <name> <data>`**

Command to update an existing policy. The JWT is set in the headers. This method requires admin rights in the Trusted Schemas Registry.

Use the `build-` prefix to call the API helper to build the transaction. If this prefix is not set then the command will perform the following steps:

- call `tsr build-updatePolicy` to build an unsigned transaction.
- call `compute signTransaction` to sign the transaction with the user defined in `using user`.
- call `tsr sendSignedTransaction` to send the transaction signed.
- call `view transactionInfo` to see relevant data for this transaction.

Params:

- **name**: Name of the policy.
- **data** (optional): JSON data to store in the policy. By default it uses random data.

Examples:

- `tsr updatePolicy my-policy {"name":"my policy"}`

**`tsr (build-)insertSchema <schema> <metadata>`**

Command to insert a new schema. The JWT is set in the headers. This method requires admin rights in the Trusted Schemas Registry. The schema id is computed based on the schema (see [schema lifecycle](https://ec.europa.eu/cefdigital/wiki/pages/viewpage.action?spaceKey=BLOCKCHAININT&title=Schema+lifecycle)).

Use the `build-` prefix to call the API helper to build the transaction. If this prefix is not set then the command will perform the following steps:

- call `tsr build-insertSchema` to build an unsigned transaction.
- call `compute signTransaction` to sign the transaction with the user defined in `using user`.
- call `tsr sendSignedTransaction` to send the transaction signed.
- call `view transactionInfo` to see relevant data for this transaction.

Params:

- **schema** (optional): JSON schema. By default it uses a random schema.
- **metadata** (optional): JSON metadata. By default is uses random metadata.

Examples:

- `tsr insertSchema`
- `tsr insertSchema {"@context":"ebsi.eu","type":"schema"} {"metadata":"data"}`

**`tsr (build-)updateSchema <schemaId> <schema> <metadata>`**

Command to update an schema. The JWT is set in the headers. This method requires admin rights in the Trusted Schemas Registry.

Use the `build-` prefix to call the API helper to build the transaction. If this prefix is not set then the command will perform the following steps:

- call `tsr build-updateSchema` to build an unsigned transaction.
- call `compute signTransaction` to sign the transaction with the user defined in `using user`.
- call `tsr sendSignedTransaction` to send the transaction signed.
- call `view transactionInfo` to see relevant data for this transaction.

Params:

- **schemaId**: Schema ID.
- **schema** (optional): JSON schema. By default it uses a random schema.
- **metadata** (optional): JSON metadata. By default is uses random metadata.

Examples:

- `tsr updateSchema`
- `tsr updateSchema 0x12f... {"@context":"ebsi.eu","type":"schema"} {"metadata":"data"}`

**`tsr sendSignedTransaction <unsignedTransaction> <signedTransaction>`**

Command to send a signed transaction to the Trusted Schemas Registry. The JWT is set in the headers. It's also possible to load the parameters from the saved variables by referencing the names.

Params:

- **unsignedTransaction**: JSON with the unsigned transaction.
- **signedTransaction**: Signed transaction in raw format.

Examples:

- `tsr sendSignedTransaction {"from":"0x6b...} 0xf9022...`
- `tsr sendSignedTransaction utx sgntx`

## Storage API <a name="storage-api"></a>

OpenAPI documentation: https://api-pilot.ebsi.eu/docs/apis/storage/latest

**`storage get <url>`**

Command to GET in the Storage API. The JWT is set in the headers.

Params:

- **url**: Path to get. In the case of files they will be downloaded in the folder /downloads.

Examples:

- `storage get /stores/distributed`
- `storage get /stores/distributed/files`
- `storage get /stores/distributed/files/0x72...ede`
- `storage get /stores/distributed/files/0x72...ede/metadata`
- `storage get /stores/distributed/key-values`
- `storage get /stores/distributed/key-values/my-key`

**`storage file insert <filename> <metadata>`**

Command to upload a file in the Storage API. The JWT is set in the headers.

Params:

- **filename** (optional): path to file to upload. By default it creates a random file.
- **metadata** (optional): JSON with metadata.

Examples:

- `storage file insert`
- `storage file insert report.pdf`
- `storage file insert C:\report.pdf {"date":"2021-07-07"}`

**`storage file patch <hash> <patchOpts>`**

Command to patch the metadata of a file. The JWT is set in the headers.

Params:

- **hash**: Hash of the file.
- **patchOpts** (optional): Array with patch options. By default it updates the metadata with random data. It's important to note that the file can not be updated.

Examples:

- `storage file patch 0x162084e...f75b2d69c`
- `storage file patch 0x162084e...f75b2d69c {"date":"2021-07-07"}`

**`storage file delete <hash>`**

Command to delete a file. The JWT is set in the headers.

Params:

- **hash**: Hash of the file to delete.

Examples:

- `storage file delete 0x162084e...f75b2d69c`

**`storage keyvalue insert <key> <value>`**

Command to insert a key-value in the Storage API. The JWT is set in the headers.

Params:

- **key** (optional): Key identificer. By default it creates a random key.
- **value** (optional): String value. By default it creates a random string.

Examples:

- `storage keyvalue insert`
- `storage keyvalue insert my-key`
- `storage keyvalue insert my-key my-value`

**`storage keyvalue update <key> <value>`**

Command to update a key in the Storage API. The JWT is set in the headers.

Params:

- **key**: Key identificer.
- **value** (optional): New value. By default it creates a random string.

Examples:

- `storage keyvalue update`
- `storage keyvalue update my-key`
- `storage keyvalue update my-key my-value`

**`storage keyvalue delete <key>`**

Command to delete a key. The JWT is set in the headers.

Params:

- **key**: Key to delete.

Examples:

- `storage keyvalue delete my-key`

## Proxy Data Hub API <a name="proxy-data-hub-api"></a>

OpenAPI documentation: https://api-pilot.ebsi.eu/docs/apis/proxy-data-hub/latest

**`datahub get <url>`**

Command to GET in the Proxy Data Hub API. The JWT is set in the headers.

Params:

- **url**: Path to get.

Examples:

- `datahub get /attributes`
- `datahub get /attributes/162084e...f75b2d69c`

**`datahub insert <data> <visibility> <sharedWith>`**

Command to insert attributes in the Proxy Data Hub API. The JWT is set in the headers.

Params:

- **data** (optional): JSON data to store. By default it creates random data.
- **visibility** (optional): It should be "private" (default) or "shared".
- **sharedWith** (optional): DID with whom it is shared. By default it is empty.

Examples:

- `datahub insert`
- `datahub insert {"name":"alice"} private`
- `datahub insert {"name":"alice"} shared` (shared with everyone)
- `datahub insert {"name":"alice"} shared did:ebsi:zkqR9GCLrLYbkubAjuqQZAz`

**`datahub patch <attributeId> <patchOpts>`**

Command to patch an attribute. The JWT is set in the headers.

Params:

- **attributeId**: Attribute ID.
- **patchOpts** (optional): Array with patch options. By default it updates the contentType to "application/ld+json". It's important to note that data and did can not be updated.

Examples:

- `datahub patch 162084e...f75b2d69c`
- `datahub patch 162084e...f75b2d69c [{"op":"replace","path":"/contentType","value":"application/ld+json"}]`

**`datahub delete <attributeId>`**

Command to delete an attribute. The JWT is set in the headers.

Params:

- **attributeId**: Attribute ID to delete.

Examples:

- `datahub delete 162084e...f75b2d69c`

## Notifications API <a name="notifications-api"></a>

OpenAPI documentation: https://api-pilot.ebsi.eu/docs/apis/notifications/latest

**`notifications get <url>`**

Command to GET in the Notifications API. The JWT is set in the headers.

Params:

- **url**: Path to get.

Examples:

- `notifications get /notifications`
- `notifications get /notifications/1fdd...ef0b`

**`notifications insert <to> <payload>`**

Command to insert a JSON-LD notification. The notification is signed by the user defined in `using user`. The JWT is set in the headers.

Params:

- **to**: DID of the receiver.
- **payload** (optional): JSON-LD notification. By default it creates a random JSON-LD.

Examples:

- `notifications insert did:ebsi:zkqR9GCLrLYbkubAjuqQZAz`
- `notifications insert did:ebsi:zkqR9GCLrLYbkubAjuqQZAz {"@context":"ebsi.eu","data":"test"}`

**`notifications delete <id>`**

Command to delete a notification. The JWT is set in the headers.

Params:

- **id**: Notification ID to delete.

Examples:

- `notifications delete 162084e...f75b2d69c`

## Cassandra <a name="cassandra"></a>

Communication with cassandra db. This command requires authentication. See the env variables for more details.

**`cassandra query <query>`**

Command to make an SQL query to cassandra.

Params:

- **query**: Array containing the query following with the params.

Examples:

- `cassandra query ["select * from attribute_storage where hash = ?", "7f71748b5385647733d837f21b2d6c46427d1f364cb82f7ca5729b97f9fabd91"]`

## Wallet Conformance Testing Report <a name="wct-report"></a>

The CLI tool can be used to generate reports for Wallet Conformance Testing (to be used by Support Office).

**`wct loadReport <filename>`**

Command to load a JSON report.

Params:

- **filename**: Path to the JSON report.

Examples:

- `data: wct loadReport myreport.json`

**`wct check <report>`**

Command to present the results of a report. It presents the summary of the report with number to success and failed tests, the conformance Id, and date of test.

Examples:

- `wct check data` (where `data` is the data saved from conformance logs)

## Old endpoints <a name="old-endpoints"></a>

The CLI tool supports the connection to the previous API versions. To access these endpoints use the suffix `-old` in the command name.

Example:

```
==> tar get /apps
GET https://api-pilot.ebsi.eu/trusted-apps-registry/v3/apps
...

==> tar-old get /apps
GET https://api-pilot.ebsi.eu/trusted-apps-registry/v2/apps
...
```

You can also use this suffix for previous authentication with onboarding api and authorisation api

```
==> authorisation-old siop
```

# Programs <a name="programs"></a>

There is a set of programs that can be use a complement to the CLI tool.

## App Registration

This program can be used by Support Office to register new apps and their keys in the Trusted Apps Registry. It requires one argument: The json file containing the appAdministrator and the public keys of each app. These env variables should be defined with the credentials of Support Office to perform the transactions: `EBSI_ENV`, `PILOT_PROGRAMS_ADMIN_DID`, `PILOT_PROGRAMS_ADMIN_KEY_ID`, and `PILOT_PROGRAMS_ADMIN_PRIVATE_KEY`.

First build the code:

```
yarn build
```

then run the program to register apps:

```
yarn program:appRegistration keysTestLux01.json
```

# Prometheus <a name="prometheus"></a>

To start the server with prometheus define the following env variables:

- `DOMAIN`: domain exposing the different apis.
- `PORT`: port for prometheus api.

Then start docker with:

```
docker-compose up --build
```

Alternatively it can be started without docker:

```
yarn build
yarn prometheus
```

A server will start in `http://localhost:<port>`. Two endpoint are available:

- /metrics - To get prometheus metrics.
- /health - To get true or false depending of the health of all apis.

# Testing <a name="testing"></a>

Create a `.env` file taking `.env.example` as reference and set the corresponding variables to run e2e tests on Pilot or Test environment.

Run all the e2e tests:

```
yarn test
```

Run essential tests to quickly check the status of the network:

```
yarn test:essentials
```

Run tests by component:

```
yarn test:authorisation
yarn test:onboarding
yarn test:ledger
yarn test:did
yarn test:timestamp
yarn test:tar
yarn test:tir
yarn test:tsr
yarn test:storage
yarn test:datahub
yarn test:notifications
```

# License <a name="license"></a>

Copyright (c) 2019 European Commission
Licensed under the EUPL, Version 1.2 or - as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
You may not use this work except in compliance with the Licence.
You may obtain a copy of the Licence at:

- <https://joinup.ec.europa.eu/page/eupl-text-11-12>

Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the Licence for the specific language governing permissions and limitations under the Licence.
