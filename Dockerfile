FROM node:14.18.0-alpine3.14@sha256:908418a1a2cd63727bf28e9289f2fd053e99fc6102f6a0a6a64574273142efb2 as base
WORKDIR /app
COPY package.json yarn.lock ./
COPY patches ./patches
RUN yarn install --frozen-lockfile --silent --production && yarn cache clean

FROM base as builder
RUN yarn install --frozen-lockfile --silent && yarn cache clean
COPY tsconfig*.json ./
COPY src src
COPY prometheus prometheus
RUN yarn build

FROM base
WORKDIR /app
ENV NODE_ENV=production
COPY --from=builder /app/dist dist
RUN chown node:node /app
USER node
CMD [ "node", "dist/prometheus/index" ]