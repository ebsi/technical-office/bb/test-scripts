module.exports = {
  root: true,
  extends: [
    "airbnb-base",
    "airbnb-typescript/base",
    "plugin:@typescript-eslint/recommended",
    "plugin:@typescript-eslint/recommended-requiring-type-checking",
    "plugin:prettier/recommended",
    "prettier",
  ],
  parserOptions: {
    project: "./tsconfig.eslint.json",
  },
  env: {
    es6: true,
  },
  rules: {
    "no-console": "off",
    "no-await-in-loop": "off",
    "no-bitwise": "off",
  },
};
