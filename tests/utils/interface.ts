export type JestMockConsole = jest.SpyInstance<
  void,
  [message?: string, ...optionalParams: unknown[]]
>;
