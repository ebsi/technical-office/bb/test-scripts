import { Config } from "../../src/config";
import { AkeResponse, TrustedApp } from "../../src/interfaces";
import { JestMockConsole } from "./interface";
import { consoleOutput } from "./utils";

/* eslint-disable prefer-destructuring */
export function jestOauth2Authentication(
  app: TrustedApp,
  config: Config,
  mockConsole: JestMockConsole
): void {
  const dataString = consoleOutput<string>(mockConsole, -1, false);
  expect(dataString).toBeJsonString();
  const data = JSON.parse(dataString) as unknown;
  expect(data).toStrictEqual({
    ake1_enc_payload: expect.any(String) as string,
    ake1_sig_payload: expect.objectContaining({
      iat: expect.any(Number) as number,
      exp: expect.any(Number) as number,
      ake1_nonce: expect.any(String) as string,
      ake1_enc_payload: expect.any(String) as string,
      kid: app.kid,
      iss: expect.stringContaining(
        config.api.authorisationV2.genericName
      ) as string,
    }) as unknown,
    ake1_jws_detached: expect.stringContaining("..") as string,
    kid: expect.stringContaining(
      config.api.authorisationV2.genericKid
    ) as string,
  });
}

export function jestSiopAuthentication(
  didUser: string,
  config: Config,
  mockConsole: JestMockConsole
): void {
  let i = 0;

  // checking if it is creating a verifiable presentation (natural person)
  let dataString = consoleOutput<string>(mockConsole, i, false);
  if (dataString.includes("compute createPresentationJwt")) i += 2;

  // checking response from /authentication-requests
  dataString = consoleOutput<string>(mockConsole, i + 9, false);
  expect(dataString).toStrictEqual(
    expect.stringContaining("openid://?") as string
  );

  dataString = consoleOutput<string>(mockConsole, i + 11, false);
  expect(dataString).toBe("Authentication request OK");

  dataString = consoleOutput<string>(mockConsole, i + 13, false);
  expect(dataString).toStrictEqual(expect.stringContaining("POST https"));

  // checking response from /siop-sessions
  dataString = consoleOutput<string>(mockConsole, i + 21, false);
  expect(dataString).toBeJsonString();
  const dataAkeResp = JSON.parse(dataString) as AkeResponse;
  expect(dataAkeResp).toStrictEqual({
    ake1_enc_payload: expect.any(String) as string,
    ake1_sig_payload: expect.objectContaining({
      iat: expect.any(Number) as number,
      exp: expect.any(Number) as number,
      ake1_nonce: expect.any(String) as string,
      ake1_enc_payload: expect.any(String) as string,
      did: didUser,
      iss: expect.stringContaining(
        config.api.authorisationV2.genericName
      ) as string,
    }) as unknown,
    ake1_jws_detached: expect.stringContaining("..") as string,
    kid: expect.stringContaining(
      config.api.authorisationV2.genericKid
    ) as string,
  });

  dataString = consoleOutput<string>(mockConsole, -1, false);
  expect(dataString).toStrictEqual(
    expect.stringContaining("Session Response OK")
  );
}

export function jestOnboardingAuthentication(
  didUser: string,
  config: Config,
  mockConsole: JestMockConsole
): void {
  // checking response (verifiableCredential)
  // from onboarding/v1/authentication-responses
  let dataString = consoleOutput<string>(mockConsole, 9, false);
  expect(dataString).toBeJsonString();
  const dataAuthResp = JSON.parse(dataString) as unknown;
  expect(dataAuthResp).toStrictEqual({
    verifiableCredential: expect.objectContaining({}) as unknown,
  });

  // checking verifiable presentation created by the user
  dataString = consoleOutput<string>(mockConsole, 11, false);
  expect(dataString).toBeJsonString();
  const dataVP = JSON.parse(dataString) as unknown;
  expect(dataVP).toStrictEqual(
    expect.objectContaining({
      jwtVp: expect.any(String) as string,
    })
  );

  // checking response from /authorisation/v1/siop-sessions
  dataString = consoleOutput<string>(mockConsole, 33, false);
  expect(dataString).toBeJsonString();
  const dataAkeResp = JSON.parse(dataString) as AkeResponse;
  expect(dataAkeResp).toStrictEqual({
    ake1_enc_payload: expect.any(String) as string,
    ake1_sig_payload: expect.objectContaining({
      iat: expect.any(Number) as number,
      exp: expect.any(Number) as number,
      ake1_nonce: expect.any(String) as string,
      ake1_enc_payload: expect.any(String) as string,
      did: didUser,
      iss: expect.stringContaining(
        config.api.authorisationV2.genericName
      ) as string,
    }) as unknown,
    ake1_jws_detached: expect.stringContaining("..") as string,
    kid: expect.stringContaining(
      config.api.authorisationV2.genericKid
    ) as string,
  });
}
