import { loadConfig } from "../../src/config";
import ebsiExtended from "../utils/ebsi-extended";
import { jestOnboardingAuthentication } from "../utils/auth-jest";
import { execCommand } from "../../src/app";
import { generateTokenWebAppOnboarding } from "../utils/onboarding";
import { Client } from "../../src/utils/Client";

expect.extend(ebsiExtended);
const mockConsole = jest.spyOn(console, "log");
mockConsole.mockImplementation(() => {});

const config = loadConfig();

describe("Users Onboarding (e2e)", () => {
  beforeEach(() => {
    jest.resetAllMocks();
  });

  describe.each(["ES256K", "ES256", "RS256", "EdDSA"])("alg %s", (alg) => {
    let tokenCaptcha: string;
    beforeAll(async () => {
      tokenCaptcha = await generateTokenWebAppOnboarding();
      await execCommand(`using token ${tokenCaptcha}`);
    });

    describe.each(["did1", "did2"])("did method %s", (didMethod) => {
      it(`should support the full SIOP flow for an unknown user using alg ${alg} - ${
        didMethod === "did1" ? "legal entity" : "natural person"
      }`, async () => {
        expect.assertions(alg === "RS256" ? 1 : 7);
        await execCommand("using user null");
        const client = await execCommand<Client>(
          `using user ${alg} ${didMethod}`
        );
        jest.resetAllMocks();
        const command = `onboarding authentication ${alg}`;
        if (alg === "RS256") {
          // RSA not supported in verifiable presentations for natural persons
          await expect(execCommand(command)).rejects.toThrow(
            "getKeyPairForKtyAndCrv does not support: RSA and undefined"
          );
        } else {
          const token = await execCommand(command);
          jestOnboardingAuthentication(client.did, config, mockConsole);
          expect(token).toBeJwt();
        }
      });
    });
  });
});
