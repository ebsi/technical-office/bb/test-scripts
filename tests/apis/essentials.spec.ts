import { execCommand } from "../../src/app";
import { expectCollection, expectResponse } from "../utils/api-get-jest";
import ebsiExtended from "../utils/ebsi-extended";

expect.extend(ebsiExtended);
const mockConsole = jest.spyOn(console, "log");
mockConsole.mockImplementation(() => {});

describe("Essential e2e tests", () => {
  describe.each([
    "timestamp",
    "storage",
    "ledger",
    "notifications",
    "authorisation",
    "onboarding",
    "did",
    "datahub",
    "tar",
    "tir",
    "tsr",
  ])("Health for %s", (api: string) => {
    it(`should get health`, async () => {
      expect.assertions(2);
      const info = { "ebsi-apis": { status: "up" } };
      jest.resetAllMocks();
      await execCommand(`${api} get /health`);
      expectResponse(mockConsole, {
        status: "ok",
        info,
        error: {},
        details: info,
      });
    });
  });

  describe("Hash algorithms for timestamp", () => {
    it("should get hash algorithm 0", async () => {
      expect.assertions(2);
      jest.resetAllMocks();
      await execCommand(`timestamp get /hash-algorithms/0`);
      expectResponse(mockConsole, {
        outputLengthBits: 256,
        ianaName: "sha-256",
        oid: "2.16.840.1.101.3.4.2.1",
        status: "active",
        multihash: "sha2-256",
      });
    });

    it("should get hash algorithm 1", async () => {
      expect.assertions(2);
      jest.resetAllMocks();
      await execCommand(`timestamp get /hash-algorithms/1`);
      expectResponse(mockConsole, {
        outputLengthBits: 256,
        ianaName: "sha-256",
        oid: "2.16.840.1.101.3.4.2.1",
        status: "active",
        multihash: "sha2-256",
      });
    });

    it.skip("should get hash algorithm 2", async () => {
      expect.assertions(2);
      jest.resetAllMocks();
      await execCommand(`timestamp get /hash-algorithms/2`);
      expectResponse(mockConsole, {
        outputLengthBits: 384,
        ianaName: "sha-384",
        oid: "2.16.840.1.101.3.4.2.2",
        status: "active",
        multihash: "sha2-384",
      });
    });

    it("should get hash algorithm 3", async () => {
      expect.assertions(2);
      jest.resetAllMocks();
      await execCommand(`timestamp get /hash-algorithms/3`);
      expectResponse(mockConsole, {
        outputLengthBits: 512,
        ianaName: "sha-512",
        oid: "2.16.840.1.101.3.4.2.3",
        status: "active",
        multihash: "sha2-512",
      });
    });

    it("should get hash algorithm 4", async () => {
      expect.assertions(2);
      jest.resetAllMocks();
      await execCommand(`timestamp get /hash-algorithms/4`);
      expectResponse(mockConsole, {
        outputLengthBits: 224,
        ianaName: "sha3-224",
        oid: "2.16.840.1.101.3.4.2.7",
        status: "active",
        multihash: "sha3-224",
      });
    });

    it("should get hash algorithm 5", async () => {
      expect.assertions(2);
      jest.resetAllMocks();
      await execCommand(`timestamp get /hash-algorithms/5`);
      expectResponse(mockConsole, {
        outputLengthBits: 256,
        ianaName: "sha3-256",
        oid: "2.16.840.1.101.3.4.2.8",
        status: "active",
        multihash: "sha3-256",
      });
    });

    it("should get hash algorithm 6", async () => {
      expect.assertions(2);
      jest.resetAllMocks();
      await execCommand(`timestamp get /hash-algorithms/6`);
      expectResponse(mockConsole, {
        outputLengthBits: 384,
        ianaName: "sha3-384",
        oid: "2.16.840.1.101.3.4.2.9",
        status: "active",
        multihash: "sha3-384",
      });
    });

    it("should get hash algorithm 7", async () => {
      expect.assertions(2);
      jest.resetAllMocks();
      await execCommand(`timestamp get /hash-algorithms/7`);
      expectResponse(mockConsole, {
        outputLengthBits: 512,
        ianaName: "sha3-512",
        oid: "2.16.840.1.101.3.4.2.10",
        status: "active",
        multihash: "sha3-512",
      });
    });
  });

  describe.each([
    ["timestamp", "/timestamps"],
    ["did", "/identifiers"],
    ["tar", "/apps"],
    ["tir", "/issuers"],
    ["tsr", "/schemas"],
  ])("Registry %s", (api, collection) => {
    it(`should get from ${api} the collection ${collection}`, async () => {
      expect.assertions(2);
      jest.resetAllMocks();
      await execCommand(`${api} get ${collection}`);
      expectCollection(mockConsole);
    });
  });
});
