import crypto from "crypto";
import { ethers } from "ethers";
import { loadConfig } from "../../src/config";
import { execCommand } from "../../src/app";
import {
  expectCollection,
  expectResponse,
  expectStatus,
} from "../utils/api-get-jest";
import ebsiExtended from "../utils/ebsi-extended";
import {
  expectRevertedTransaction,
  expectTransaction,
} from "../utils/jsonrpc-jest";
import { computeSchemaId } from "../../src/utils";

const { sha256 } = ethers.utils;

expect.extend(ebsiExtended);
const mockConsole = jest.spyOn(console, "log");
mockConsole.mockImplementation(() => {});

const config = loadConfig();
const writeOpsEnabled = ["test", "conformance"].includes(config.env);
const describeWriteOps = writeOpsEnabled ? describe : describe.skip;
const itOnlyTest = config.env === "test" ? it : it.skip;
const { user1, admin, requesterApp } = config.jest;

describe("Trusted Schemas Registry (e2e)", () => {
  beforeAll(async () => {
    // siop session for the user
    await execCommand(
      `using user ES256K did1 ${user1.privateKey} ${user1.did} ${user1.keyId}`
    );
    await execCommand("tokenUser: authorisation-old siop");

    if (writeOpsEnabled) {
      if (!config.besuProvider) {
        // oauth2 session to access ledger api
        await execCommand(
          `using app ${requesterApp.name} ${requesterApp.privateKey}`
        );
        await execCommand("tokenLedger: authorisation-old oauth2 ledger-api");
      }

      // siop session for the admin
      await execCommand("using user null");
      await execCommand(
        `using user ES256K did1 ${admin.privateKey} ${admin.did} ${admin.keyId}`
      );
      await execCommand("tokenAdmin: authorisation-old siop");
    }
  });

  describe("GET /schemas", () => {
    let schemaId: string;
    let schemaRevisionId: string;
    let metadataId: string;
    it("should get a collection of schemas", async () => {
      expect.assertions(3);
      jest.resetAllMocks();
      await execCommand("tsr get /schemas");
      const data = expectCollection<{
        schemaId: string;
        href: string;
      }>(mockConsole);
      expectStatus(mockConsole, 200);
      schemaId = data.items[0].schemaId;
    });

    it("should get a schema", async () => {
      expect.assertions(3);
      jest.resetAllMocks();
      await execCommand(`tsr get /schemas/${schemaId}`);
      expectResponse(mockConsole, expect.objectContaining({}));
      expectStatus(mockConsole, 200);
    });

    it("should get the revisions of a schema", async () => {
      expect.assertions(3);
      jest.resetAllMocks();
      await execCommand(`tsr get /schemas/${schemaId}/revisions`);
      const data = expectCollection<{
        schemaRevisionId: string;
        href: string;
      }>(mockConsole);
      expectStatus(mockConsole, 200);
      schemaRevisionId = data.items[0].schemaRevisionId;
    });

    it("should get a revision from a schema", async () => {
      expect.assertions(3);
      jest.resetAllMocks();
      await execCommand(
        `tsr get /schemas/${schemaId}/revisions/${schemaRevisionId}`
      );
      expectResponse(mockConsole, expect.objectContaining({}));
      expectStatus(mockConsole, 200);
    });

    it("should get metadata collection from a revision from a schema", async () => {
      expect.assertions(3);
      jest.resetAllMocks();
      await execCommand(
        `tsr get /schemas/${schemaId}/revisions/${schemaRevisionId}/metadata`
      );
      const data = expectCollection<{
        metadataId: string;
        href: string;
      }>(mockConsole);
      expectStatus(mockConsole, 200);
      metadataId = data.items[0].metadataId;
    });

    it("should get the metadata content from a revision from a schema", async () => {
      expect.assertions(3);
      jest.resetAllMocks();
      await execCommand(
        `tsr get /schemas/${schemaId}/revisions/${schemaRevisionId}/metadata/${metadataId}`
      );
      expectResponse(mockConsole, expect.objectContaining({}));
      expectStatus(mockConsole, 200);
    });
  });

  describe("GET /policies", () => {
    let policyId: string;
    it("should get a collection of policies", async () => {
      expect.assertions(3);
      jest.resetAllMocks();
      await execCommand("tsr get /policies");
      const data = expectCollection<{
        policyId: string;
        href: string;
      }>(mockConsole);
      expectStatus(mockConsole, 200);
      if (data.items.length) policyId = data.items[0].policyId;
    });

    itOnlyTest("should get a policy", async () => {
      expect.assertions(3);
      jest.resetAllMocks();
      await execCommand(`tsr get /policies/${policyId}`);
      expectResponse(mockConsole, {
        policyId,
        policy: expect.any(String) as string,
        hash: expect.any(String) as string,
      });
      expectStatus(mockConsole, 200);
    });

    itOnlyTest("should get the revisions of a policy", async () => {
      expect.assertions(3);
      jest.resetAllMocks();
      await execCommand(`tsr get /policies/${policyId}/revisions`);
      expectCollection<{
        policyId: string;
        policy: string;
        hash: string;
      }>(mockConsole);
      expectStatus(mockConsole, 200);
    });
  });

  describeWriteOps("POST /jsonrpc", () => {
    const policyId = `policy-${crypto.randomBytes(10).toString("hex")}`;
    const schema = {
      "@context": "https://ebsi.eu",
      type: "Schema",
      name: "example",
      title: "test schema",
      data: crypto.randomBytes(16).toString("hex"),
    };
    const schema2 = {
      ...schema,
      title: "test schema v2",
    };
    let schemaId: string;
    const serializedSchema = JSON.stringify(schema);
    const serializedSchema2 = JSON.stringify(schema2);
    const serializedSchemaBuffer = Buffer.from(serializedSchema);
    const schemaRevisionId = sha256(serializedSchemaBuffer);

    const protectedCommands = [
      ["insertSchema", () => JSON.stringify(schema)],
      ["updateSchema", () => `${schemaId} ${serializedSchema2}`],
      ["updateMetadata", () => `${schemaRevisionId}`],
    ];

    const commands = [
      ...protectedCommands,
      ["insertPolicy", () => policyId],
      ["updatePolicy", () => policyId],
    ];

    describe.each(protectedCommands)(
      "Restricted user access when using %s",
      (method: string, params: () => string) => {
        beforeAll(async () => {
          await execCommand("using user null");
          await execCommand(
            `using user ES256K did1 ${user1.privateKey} ${user1.did} ${user1.keyId}`
          );
          await execCommand("using token tokenUser");
          schemaId = await computeSchemaId(schema, "base16");
        });
        it("should reject not authorized users", async () => {
          expect.assertions(8);
          jest.resetAllMocks();
          await expect(
            execCommand(`tsr ${method} ${params()}`)
          ).rejects.toThrow();
          expectRevertedTransaction(
            mockConsole,
            `Policy error: sender doesn't have the attribute TSR:${method}`
          );
        });
      }
    );

    describe.each(commands)(
      "send transaction for %s",
      (method: string, params: () => string) => {
        beforeAll(async () => {
          await execCommand("using user null");
          await execCommand(
            `using user ES256K did1 ${admin.privateKey} ${admin.did} ${admin.keyId}`
          );
          await execCommand("using token tokenAdmin");
          await execCommand("using oauth2token tokenLedger");
          schemaId = await computeSchemaId(schema, "base16");
        });
        it("should work", async () => {
          expect.assertions(7);
          jest.resetAllMocks();
          await execCommand(`tsr ${method} ${params()}`);
          expectTransaction(mockConsole);
        });
      }
    );
  });
});
