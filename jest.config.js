module.exports = {
  preset: "ts-jest",
  testTimeout: 600_000,
  maxConcurrency: 1,
  testEnvironment: "node",
  rootDir: ".",
  roots: ["<rootDir>/tests/"],
  testMatch: ["**/?(*.|*-)+(spec|test).ts"],
  transform: {
    "^.+\\.(t|j)s$": "ts-jest",
  },
  transformIgnorePatterns: [],
  moduleFileExtensions: ["js", "json", "ts"],
  moduleNameMapper: {
    "^jose/(.*)$":
      "<rootDir>/node_modules/@cef-ebsi/siop-auth/node_modules/jose/dist/node/cjs/$1",
  },
  reporters: [
    "default",
    [
      "./node_modules/jest-html-reporter",
      {
        pageTitle: "Test Report",
        outputPath: "./reports/test-report.html",
        includeFailureMsg: true,
      },
    ],
  ],
  globals: {
    "ts-jest": {
      diagnostics: true,
      warnOnly: true,
      ignoreCodes: [
        18002, // The ‘files’ list in config file is empty. (it is strongly recommended to include this one)
      ],
      pretty: true,
    },
  },
};
