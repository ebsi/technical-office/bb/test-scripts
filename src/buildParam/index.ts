import { Client } from "../utils/Client";
import { BuildParamResponse, UnknownObject } from "../interfaces";
import { buildParamDid } from "./did";
import { buildParamDidV3 } from "./didV3";
import { buildParamDidOld } from "./didOld";
import { buildParamTar } from "./tar";
import { buildParamTarV3 } from "./tarV3";
import { buildParamTimestamp } from "./timestamp";
import { buildParamTimestampV2 } from "./timestampV2";
import { buildParamTir } from "./tir";
import { buildParamTirV3 } from "./tirV3";
import { buildParamTsr } from "./tsr";
import { buildParamTsrV2 } from "./tsrV2";
import { buildParamTpr } from "./tpr";
import { buildParamTprV2 } from "./tprV2";

export async function buildParam(
  contract: string,
  method: string,
  client: Client,
  inputs: (string | UnknownObject)[]
): Promise<BuildParamResponse> {
  switch (contract) {
    case "timestamp":
      return buildParamTimestamp(method, client, inputs);
    case "timestamp-new":
      return buildParamTimestampV2(method, client, inputs);
    case "did":
      return buildParamDid(method, client, inputs);
    case "did-new":
      return buildParamDidV3(method, client, inputs);
    case "did-old":
      return buildParamDidOld(method, client, inputs);
    case "tar":
      return buildParamTar(method, client, inputs);
    case "tar-new":
      return buildParamTarV3(method, client, inputs);
    case "tir":
    case "tir-old":
      return buildParamTir(method, client, inputs);
    case "tir-new":
      return buildParamTirV3(method, client, inputs);
    case "tsr":
      return buildParamTsr(method, client, inputs);
    case "tsr-new":
      return buildParamTsrV2(method, client, inputs);
    case "tpr":
      return buildParamTpr(method, client, inputs);
    case "tpr-new":
      return buildParamTprV2(method, client, inputs);
    default:
      throw new Error(`Invalid contract '${contract}'`);
  }
}

export * from "./did";
export * from "./didOld";
export * from "./tar";
export * from "./tir";
export * from "./tsr";
export * from "./tpr";
export * from "./timestamp";
