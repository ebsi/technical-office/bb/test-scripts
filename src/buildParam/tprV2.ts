import Joi from "joi";
import { BuildParamResponse } from "../interfaces/shared";
import { Client } from "../utils/Client";

export function buildParamTprV2(
  method: string,
  client: Client,
  inputs: unknown[]
): BuildParamResponse {
  switch (method) {
    case "insertUserAttributes": {
      const [user, attributes] = inputs as [string, string[]];
      Joi.assert(user, Joi.string());
      Joi.assert(attributes, Joi.array().items(Joi.string()));

      return {
        info: {
          title: `Insert User Attribute`,
          data: { user, attributes },
        },
        param: { user, attributes },
      };
    }
    case "deleteUserAttribute": {
      const [user, attribute] = inputs as string[];
      Joi.assert(user, Joi.string());
      Joi.assert(attribute, Joi.string());

      return {
        info: {
          title: `Delete User Attribute`,
          data: { user, attribute },
        },
        param: { user, attribute },
      };
    }

    case "insertPolicy": {
      const [policyName, descriptionInput] = inputs as [
        string,
        string | string[]
      ];
      Joi.assert(policyName, Joi.string());
      const description = Array.isArray(descriptionInput)
        ? descriptionInput[0]
        : descriptionInput;

      return {
        info: {
          title: "Insert Policy",
          data: { policyName, description },
        },
        param: { policyName, description },
      };
    }

    case "updatePolicy": {
      const [policyName, descriptionInput] = inputs as [
        string,
        string | string[]
      ];
      Joi.assert(policyName, Joi.string());
      const description = Array.isArray(descriptionInput)
        ? descriptionInput[0]
        : descriptionInput;

      return {
        info: {
          title: "Insert Policy",
          data: { policyName, description },
        },
        param: { policyName, description },
      };
    }

    case "deactivatePolicy":
    case "activatePolicy": {
      const [policyIdorName] = inputs as string[];
      Joi.assert(policyIdorName, Joi.string());

      const isId = !Number.isNaN(Number(policyIdorName));
      const policyRef = {
        ...(isId && { policyId: policyIdorName }),
        ...(!isId && { policyName: policyIdorName }),
      };

      return {
        info: { title: "Deactivate Policy", data: policyRef },
        param: policyRef,
        method: isId ? `${method}(uint256)` : `${method}(string)`,
      };
    }
    default:
      throw new Error(`Invalid method '${method}'`);
  }
}

export default buildParamTprV2;
