import { Client } from "../utils";
import { Config } from "../config";

export interface Context {
  config: Config;
  httpOpts: {
    headers: {
      authorization?: string;
      conformance?: string;
    };
  };
  client: Client;
  rtVars: {
    [x: string]: unknown;
  };
}
