import fs from "fs";
import { ethers } from "ethers";
import { execCommand } from "../src/app";
import { loadConfig } from "../src/config";

const config = loadConfig();
const { admin } = config.programs;

const [jsonFile] = process.argv.slice(2);

const nodeKeys = JSON.parse(fs.readFileSync(jsonFile, "utf8")) as {
  appAdmin: string;
  apps: {
    name: string;
    publicKeyPemBase64: string;
  }[];
};

(async () => {
  const errors = [];
  const skipped = [];
  const success = [];

  await execCommand(
    `using user ES256K did1 ${admin.privateKey} ${admin.did} ${admin.keyId}`
  );
  await execCommand("tokenAdmin: authorisation-old siop");
  await execCommand("using token tokenAdmin");

  for (let i = 0; i < nodeKeys.apps.length; i += 1) {
    const app = nodeKeys.apps[i];

    let action: string;
    let registeredApp: { publicKeys: string[] };
    try {
      action = `insert app ${app.name}`;
      try {
        registeredApp = await execCommand(`tar get /apps/${app.name}`);
        skipped.push(`app ${app.name} already exists`);
      } catch (error) {
        // not found
        const genericApp = app.name.split("_")[0];
        if (!config.genericApps.includes(genericApp)) {
          throw new Error(`Invalid app name ${app.name}`);
        }

        await execCommand(`tar insertApp ${app.name} ${nodeKeys.appAdmin}`);
        success.push(`app ${app.name} registered`);
        registeredApp = { publicKeys: [] };
      }

      action = `register public key of ${app.name}`;
      if (registeredApp.publicKeys.includes(app.publicKeyPemBase64)) {
        skipped.push(`public key for ${app.name} already exists`);
      } else {
        const appId = ethers.utils.sha256(ethers.utils.toUtf8Bytes(app.name));
        await execCommand(
          `tar insertAppPublicKey ${appId} ${app.publicKeyPemBase64}`
        );
        success.push(`public key for ${app.name} registered`);
      }
    } catch (error) {
      errors.push({
        action,
        error: (error as Error).message,
      });
    }
  }
  console.log(`\n\nApp admin: ${nodeKeys.appAdmin}`);
  console.log(`\nSuccess: ${success.length}`);
  console.log(success);
  console.log(`\nSkipped: ${skipped.length}`);
  console.log(skipped);
  console.log(`\nErrors: ${errors.length}`);
  console.log(errors);
})()
  .then(() => {
    process.exit(0);
  })
  .catch((error) => {
    console.log(error);
    process.exit(1);
  });
